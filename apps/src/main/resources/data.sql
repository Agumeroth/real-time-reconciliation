SELECT 1 FROM dual;

insert into rec_object_type (object_type_id, object_type_name, derivation_logic,
excluded_fields, enabled_flag,
creation_date, last_update_date) values
(1, 'invoiceHeader', '$.invoiceHeader',
'invoiceLines', 'Y',
sysdate, sysdate);

insert into rec_object_type (object_type_id, object_type_name, derivation_logic,
excluded_fields, enabled_flag,
creation_date, last_update_date) values
(2, 'invoiceLine', '$.invoiceHeader.invoiceLines',
null, 'Y',
sysdate, sysdate);

insert into rec_object_type (object_type_id, object_type_name, derivation_logic,
excluded_fields, enabled_flag,
creation_date, last_update_date) values
(3, 'invoiceTxnLine', '$.invoiceTxnLine',
null, 'Y',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,1,'objectIdentifier',
'objectIdentifier', 'STRING', '$.invoiceHeader.invoiceNumber',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,1,'version',
'version', 'NUMBER', '$.invoiceHeader.changeNumber',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,1,'lineCount',
'flexField1', 'NUMBER', '$.invoiceHeader.lineCount',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,1,'invoiceType',
'flexField2', 'STRING', '$.invoiceHeader.invoiceType',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,1,'currencyType',
'flexField3', 'STRING', '$.invoiceHeader.currencyType',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,1,'invoicingDate',
'flexField4', 'DATE', '$.invoiceHeader.invoicingDate',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,1,'orderNumber',
'flexField5', 'STRING', '$.invoiceHeader.orderNumber',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,1,'customer',
'flexField6', 'STRING', '$.invoiceHeader.customer',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,1,'totalAmount',
'flexField7', 'NUMBER', '$.invoiceHeader.totalAmount',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,1,'paymentTerm',
'flexField8', 'STRING', '$.invoiceHeader.paymentTerm',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,1,'dueDate',
'flexField9', 'DATE', '$.invoiceHeader.dueDate',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,1,'orgCode',
'flexField10', 'STRING', '$.invoiceHeader.orgCode',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,2,'objectIdentifier',
'objectIdentifier', 'NUMBER', '$.invoiceHeader.invoiceLines.[?].lineNumber',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,2,'parentIdentifier',
'parentIdentifier', 'STRING', '$.invoiceHeader.invoiceNumber',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,2,'version',
'version', 'NUMBER', '$.invoiceHeader.invoiceLines.[?].changeNumber',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,2,'itemId',
'flexField1', 'STRING', '$.invoiceHeader.invoiceLines.[?].itemId',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,2,'description',
'flexField2', 'STRING', '$.invoiceHeader.invoiceLines.[?].description',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,2,'quantity',
'flexField3', 'NUMBER', '$.invoiceHeader.invoiceLines.[?].quantity',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,2,'uom',
'flexField4', 'STRING', '$.invoiceHeader.invoiceLines.[?].uom',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,2,'unitPrice',
'flexField5', 'NUMBER', '$.invoiceHeader.invoiceLines.[?].unitPrice',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,2,'amount',
'flexField6', 'NUMBER', '$.invoiceHeader.invoiceLines.[?].amount',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,2,'orderLineId',
'flexField7', 'STRING', '$.invoiceHeader.invoiceLines.[?].orderLineId',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,2,'itemType',
'flexField8', 'STRING', '$.invoiceHeader.invoiceLines.[?].itemType',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,3,'objectIdentifier',
'objectIdentifier', 'NUMBER', '$.invoiceTxnLine.lineNumber',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,3,'parentIdentifier',
'parentIdentifier', 'STRING', '$.invoiceTxnLine.invoiceNumber',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,3,'version',
'version', 'NUMBER', '$.invoiceTxnLine.changeNumber',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,3,'txnAmount',
'flexField1', 'NUMBER', '$.invoiceTxnLine.txnAmount',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,3,'txnType',
'flexField2', 'STRING', '$.invoiceTxnLine.txnType',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,3,'processedDate',
'flexField3', 'DATE', '$.invoiceTxnLine.processedDate',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,3,'dueDate',
'flexField4', 'DATE', '$.invoiceTxnLine.dueDate',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,3,'invoicingDate',
'flexField5', 'DATE', '$.invoiceTxnLine.invoicingDate',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,3,'orgCode',
'flexField6', 'STRING', '$.invoiceTxnLine.orgCode',
sysdate, sysdate);

insert into rec_flex_field_definition
(flex_field_definition_id, object_type_id, object_attribute_name,
column_name, column_type, derivation_logic,
creation_date, last_update_date)
values
(REC_SEQ.nextval,3,'orderLineId',
'flexField7', 'STRING', '$.invoiceTxnLine.orderLineId',
sysdate, sysdate);

insert into rec_rule
(rule_id, rule_name, object_type_id, comparing_object_type_id, one_to_many_flag,
matching_rule_definition,
rule_definition,
creation_date, last_update_date)
values
(REC_SEQ.nextval, 'TestRule1', 1, 2, 'Y',
'invoiceHeader:objectIdentifier == invoiceLine:parentIdentifier',
'invoiceHeader:totalAmount == Sum([invoiceLine:amount])',
sysdate, sysdate);

insert into rec_rule
(rule_id, rule_name, object_type_id, comparing_object_type_id, one_to_many_flag,
matching_rule_definition,
rule_definition,
creation_date, last_update_date)
values
(REC_SEQ.nextval, 'TestRule2',1, 2, 'Y',
'invoiceHeader:objectIdentifier == invoiceLine:parentIdentifier',
'invoiceHeader:lineCount == Count([invoiceLine:objectIdentifier])',
sysdate, sysdate);

insert into rec_rule
(rule_id, rule_name, object_type_id, comparing_object_type_id, one_to_many_flag,
matching_rule_definition,
rule_definition,
creation_date, last_update_date)
values
(REC_SEQ.nextval, 'TestRule3',2, 3, 'N',
'invoiceLine:parentIdentifier == invoiceTxnLine:parentIdentifier && invoiceLine:objectIdentifier == invoiceTxnLine:objectIdentifier && invoiceTxnLine:txnType == ''INVOICE''',
'invoiceLine:amount == invoiceTxnLine:txnAmount',
sysdate, sysdate);

insert into rec_rule
(rule_id, rule_name, object_type_id, comparing_object_type_id, one_to_many_flag,
matching_rule_definition,
rule_definition,
creation_date, last_update_date)
values
(REC_SEQ.nextval, 'TestRule4',1, 3, 'Y',
'invoiceHeader:objectIdentifier == invoiceTxnLine:parentIdentifier && invoiceTxnLine:txnType == ''INVOICE''',
'invoiceHeader:lineCount == Count([invoiceTxnLine:objectIdentifier])',
sysdate, sysdate);
