package com.plaszlo.apps.infrastructure.messaging;

import static java.lang.Boolean.FALSE;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import java.util.function.Predicate;

/**
 * @author plaszlo
 */
public class BooleanDeserializer extends JsonDeserializer<Boolean> {

  private static final Predicate<String> IS_TRUE_VALUE =
      p -> "yes".equalsIgnoreCase(p) || "y".equalsIgnoreCase(p) || "true".equalsIgnoreCase(p);

  @Override
  public Boolean deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
      throws IOException {
    return jsonParser.getText() != null ? IS_TRUE_VALUE.test(jsonParser.getText().trim()) : FALSE;
  }
}
