package com.plaszlo.apps.infrastructure.persistence;

import javax.persistence.AttributeConverter;

/**
 * @author plaszlo
 */
public class BooleanToStringConverter implements AttributeConverter<Boolean, String> {

  @Override
  public String convertToDatabaseColumn(Boolean aBoolean) {
    return (aBoolean != null && aBoolean) ? "Y" : "N";
  }

  @Override
  public Boolean convertToEntityAttribute(String s) {
    return "Y".equals(s);
  }
}
