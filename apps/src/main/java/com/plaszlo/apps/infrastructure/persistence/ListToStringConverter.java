package com.plaszlo.apps.infrastructure.persistence;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.AttributeConverter;

/**
 * @author plaszlo
 */
public class ListToStringConverter implements AttributeConverter<List<Long>, String> {

  private static final String SEPARATOR = ",";

  @Override
  public String convertToDatabaseColumn(List<Long> longs) {
    if (longs == null || longs.isEmpty()) {
      return null;
    }
    List<String> stringList = new ArrayList<>(longs.size());
    for (Long element : longs) {
      stringList.add(String.valueOf(element));
    }
    return String.join(SEPARATOR, stringList);
  }

  @Override
  public List<Long> convertToEntityAttribute(String s) {
    if (s == null) {
      return new ArrayList<>();
    }
    List<String> stringList = Arrays.asList(s.split(SEPARATOR));
    List<Long> longList = new ArrayList<>(stringList.size());
    for (String element : stringList) {
      longList.add(Long.valueOf(element));
    }
    return longList;
  }
}
