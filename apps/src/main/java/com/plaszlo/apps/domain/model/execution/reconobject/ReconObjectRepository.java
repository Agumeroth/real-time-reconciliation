package com.plaszlo.apps.domain.model.execution.reconobject;

import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author plaszlo
 */
public interface ReconObjectRepository extends PagingAndSortingRepository<ReconObject, Long> {

  Optional<ReconObject> findByObjectTypeIdAndObjectIdentifier(Long id, String identifier);

  List<ReconObject> findByObjectTypeId(Long objectTypeId);

  List<ReconObject> deleteByObjectStageId(Long objectStageId);

  List<ReconObject> findByObjectStageId(Long objectStageId);
}
