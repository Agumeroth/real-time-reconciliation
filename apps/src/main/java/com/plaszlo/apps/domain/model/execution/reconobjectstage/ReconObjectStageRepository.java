package com.plaszlo.apps.domain.model.execution.reconobjectstage;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 * @author plaszlo
 */
public interface ReconObjectStageRepository extends PagingAndSortingRepository<ReconObjectStage, Long> {

  /**
   * @param topic
   * @param message
   * @return
   */
  Optional<ReconObjectStage> findByTopicAndMessage(String topic, String message);

  /**
   * @return
   */
  List<ReconObjectStage> findAll();

  /**
   * @return
   */
  @Query(value = "SELECT stage FROM ReconObjectStage stage WHERE stage.creationDate >= :startDate "
      + "AND stage.creationDate <= :endDate")
  List<ReconObjectStage> findAllBetween(@Param("startDate") Instant startDate, @Param("endDate") Instant endDate);

  /**
   * @param topic
   * @return
   */
  List<ReconObjectStage> findAllByTopic(String topic);
}
