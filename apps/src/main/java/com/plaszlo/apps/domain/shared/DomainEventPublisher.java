package com.plaszlo.apps.domain.shared;

import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

/**
 * @author plaszlo
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Component
public class DomainEventPublisher {

  private static DomainEventPublisher domainEventPublisher;

  private final ApplicationEventPublisher eventPublisher;

  @PostConstruct
  private void createDomainEventPublisher() {

    setDomainEventPublisher(this);
  }

  public static void publish(DomainEvent event) {
    DomainEventPublisher.domainEventPublisher.eventPublisher.publishEvent(event);
  }

  private static void setDomainEventPublisher(DomainEventPublisher domainEventPublisher) {
    DomainEventPublisher.domainEventPublisher = domainEventPublisher;
  }
}
