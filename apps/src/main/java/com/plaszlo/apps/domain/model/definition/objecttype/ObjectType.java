package com.plaszlo.apps.domain.model.definition.objecttype;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.plaszlo.apps.domain.shared.Auditor;
import com.plaszlo.apps.infrastructure.messaging.BooleanDeserializer;
import com.plaszlo.apps.infrastructure.persistence.BooleanToStringConverter;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

/**
 * @author plaszlo
 */
@Entity
@Table(name = "REC_OBJECT_TYPE")
@Getter
@ToString
@Builder
@Slf4j
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ObjectType extends Auditor {

  @Id
  @Column(name = "OBJECT_TYPE_ID", nullable = false)
  @SequenceGenerator(name = "REC_SEQ_GENERATOR", sequenceName = "REC_SEQ", allocationSize = 100)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REC_SEQ_GENERATOR")
  private Long id;

  @Column(name = "OBJECT_TYPE_NAME", nullable = false, length = 240)
  private String name;

  @Column(name = "DERIVATION_LOGIC", nullable = false, length = 240)
  private String derivationLogic;

  @Column(name = "EXCLUDED_FIELDS", length = 240)
  private String excludedFields;

  @Column(name = "ENABLED_FLAG", nullable = false, length = 1)
  @Convert(converter = BooleanToStringConverter.class)
  @JsonDeserialize(using = BooleanDeserializer.class)
  private boolean enabledFlag;

  private ObjectType(ObjectTypeBuilder builder) {
    this.name = builder.name;
    this.derivationLogic = builder.derivationLogic;
    this.excludedFields = builder.excludedFields;
    this.enabledFlag = builder.enabledFlag;
  }

  public void merge(ObjectType other) {
    this.name = other.getName();
    this.derivationLogic = other.getDerivationLogic();
    this.excludedFields = other.getExcludedFields();
    this.enabledFlag = other.isEnabledFlag();
  }

  public boolean isValidForCreation() {
    return StringUtils.isNotBlank(this.name)
        && StringUtils.isNotBlank(this.derivationLogic);
  }

  public static class ObjectTypeBuilder {

    public ObjectType build() {

      return new ObjectType(this);
    }
  }
}
