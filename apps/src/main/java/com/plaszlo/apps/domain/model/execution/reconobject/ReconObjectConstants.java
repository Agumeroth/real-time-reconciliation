package com.plaszlo.apps.domain.model.execution.reconobject;

/**
 * @author plaszlo
 */
public class ReconObjectConstants {

  public static final String OBJECT_IDENTIFIER = "objectIdentifier";
  public static final String PARENT_IDENTIFIER = "parentIdentifier";
  public static final String VERSION = "version";

  public static final String FLEX_FIELD1 = "flexField1";
  public static final String FLEX_FIELD2 = "flexField2";
  public static final String FLEX_FIELD3 = "flexField3";
  public static final String FLEX_FIELD4 = "flexField4";
  public static final String FLEX_FIELD5 = "flexField5";
  public static final String FLEX_FIELD6 = "flexField6";
  public static final String FLEX_FIELD7 = "flexField7";
  public static final String FLEX_FIELD8 = "flexField8";
  public static final String FLEX_FIELD9 = "flexField9";
  public static final String FLEX_FIELD10 = "flexField10";
  public static final String FLEX_FIELD11 = "flexField11";
  public static final String FLEX_FIELD12 = "flexField12";
  public static final String FLEX_FIELD13 = "flexField13";
  public static final String FLEX_FIELD14 = "flexField14";
  public static final String FLEX_FIELD15 = "flexField15";


  public static final boolean NOT_FOR_EVALUATION = false;
  public static final boolean FOR_EVALUATION = true;

  public static final boolean FROM_STAGE = true;
  public static final boolean FROM_MODEL = false;

  private ReconObjectConstants() {
    throw new IllegalStateException("Utility class");
  }
}
