package com.plaszlo.apps.domain.model.execution.reconobject;

import java.io.Serializable;

/**
 * @author plaszlo
 */
public enum StatusCode implements Serializable {
  RED, YELLOW, GREEN;

  public boolean sameValueAs(StatusCode other) {
    return this.equals(other);
  }

  @Override
  public String toString() {
    return this.name();
  }

  public static StatusCode from(Boolean value) {
    if (value == null || !value) {
      return RED;
    }
    return GREEN;
  }
}
