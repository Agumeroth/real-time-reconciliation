package com.plaszlo.apps.domain.model.execution.ruleexecution;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 * @author plaszlo
 */
public interface RuleExecutionRepository extends PagingAndSortingRepository<RuleExecution, Long> {

  @Query(value = "SELECT ex FROM RuleExecution ex WHERE ex.ruleId = :rId AND "
      + "ex.objectId = :objId AND ex.comparingObjectId IS NULL")
  Optional<RuleExecution> findPlaceholderExecution(@Param("rId") Long ruleId, @Param("objId") Long id);

  List<RuleExecution> findByRuleIdAndObjectId(Long ruleId, Long objectId);

  List<RuleExecution> findByObjectId(Long objectId);

  List<RuleExecution> deleteByObjectId(Long objectId);
}
