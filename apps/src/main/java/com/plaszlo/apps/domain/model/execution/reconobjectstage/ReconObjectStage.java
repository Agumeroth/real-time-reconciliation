package com.plaszlo.apps.domain.model.execution.reconobjectstage;

import com.plaszlo.apps.domain.shared.Auditor;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.Validate;

/**
 * @author plaszlo
 */
@Entity
@Table(name = "REC_OBJECT_STAGE")
@Getter
@ToString
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ReconObjectStage extends Auditor {

  @Id
  @Column(name = "OBJECT_STAGE_ID")
  @SequenceGenerator(name = "REC_SEQ_GENERATOR", sequenceName = "REC_SEQ", allocationSize = 100)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REC_SEQ_GENERATOR")
  private Long id;

  @Column(name = "TOPIC", length = 100)
  private String topic;

  @Column(name = "MESSAGE", length = 4000)
  private String message;

  private ReconObjectStage(ReconObjectStageBuilder builder) {
    this.topic = builder.topic;
    this.message = builder.message;
  }

  public static class ReconObjectStageBuilder {

    public ReconObjectStage build() {
      Validate.notBlank(this.topic, "Topic can not be blank!");
      Validate.notBlank(this.message, "Message can not be blank!");

      return new ReconObjectStage(this);
    }
  }
}
