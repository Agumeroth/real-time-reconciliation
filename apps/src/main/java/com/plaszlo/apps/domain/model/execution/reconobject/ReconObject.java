package com.plaszlo.apps.domain.model.execution.reconobject;

import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD1;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD10;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD11;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD12;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD13;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD14;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD15;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD2;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD3;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD4;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD5;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD6;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD7;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD8;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD9;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.OBJECT_IDENTIFIER;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.PARENT_IDENTIFIER;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.VERSION;
import static com.plaszlo.apps.domain.model.execution.reconobject.StatusCode.GREEN;
import static com.plaszlo.apps.domain.model.execution.reconobject.StatusCode.RED;

import com.plaszlo.apps.domain.model.definition.flexfielddefinition.FlexFieldType;
import com.plaszlo.apps.domain.model.execution.ruleexecution.RuleExecution;
import com.plaszlo.apps.domain.shared.Auditor;
import java.util.List;
import java.util.function.Predicate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.Validate;

/**
 * @author plaszlo
 */
@Entity
@Table(name = "REC_OBJECT")
@ToString
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Slf4j
public class ReconObject extends Auditor {

  @Id
  @Column(name = "OBJECT_ID", nullable = false)
  @SequenceGenerator(name = "REC_SEQ_GENERATOR", sequenceName = "REC_SEQ", allocationSize = 100)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REC_SEQ_GENERATOR")
  @Getter
  private Long id;

  @Column(name = "OBJECT_STAGE_ID", nullable = false)
  private Long objectStageId;

  @Column(name = "OBJECT_TYPE_ID", nullable = false)
  @Getter
  private Long objectTypeId;

  @Column(name = "MESSAGE", length = 4000)
  private String message;

  @Column(name = "OBJECT_IDENTIFIER", nullable = false)
  private String objectIdentifier;

  @Column(name = "OBJECT_IDENTIFIER_TYPE")
  @Enumerated(EnumType.STRING)
  private FlexFieldType objectIdentifierType;

  @Column(name = "PARENT_IDENTIFIER")
  private String parentIdentifier;

  @Column(name = "PARENT_IDENTIFIER_TYPE")
  @Enumerated(EnumType.STRING)
  private FlexFieldType parentIdentifierType;

  @Column(name = "OBJECT_VERSION", nullable = false)
  private String objectVersion;

  @Column(name = "OBJECT_VERSION_TYPE")
  @Enumerated(EnumType.STRING)
  private FlexFieldType versionType;

  @Column(name = "STATUS_CODE", nullable = false)
  @Enumerated(EnumType.STRING)
  private StatusCode statusCode;

  @Column(name = "STATUS_MESSAGE")
  private String statusMessage;

  @Column(name = "FLEX_FIELD1")
  private String flexField1;

  @Column(name = "FLEX_FIELD_TYPE1")
  @Enumerated(EnumType.STRING)
  private FlexFieldType flexFieldType1;

  @Column(name = "FLEX_FIELD2")
  private String flexField2;

  @Column(name = "FLEX_FIELD_TYPE2")
  @Enumerated(EnumType.STRING)
  private FlexFieldType flexFieldType2;

  @Column(name = "FLEX_FIELD3")
  private String flexField3;

  @Column(name = "FLEX_FIELD_TYPE3")
  @Enumerated(EnumType.STRING)
  private FlexFieldType flexFieldType3;

  @Column(name = "FLEX_FIELD4")
  private String flexField4;

  @Column(name = "FLEX_FIELD_TYPE4")
  @Enumerated(EnumType.STRING)
  private FlexFieldType flexFieldType4;

  @Column(name = "FLEX_FIELD5")
  private String flexField5;

  @Column(name = "FLEX_FIELD_TYPE5")
  @Enumerated(EnumType.STRING)
  private FlexFieldType flexFieldType5;

  @Column(name = "FLEX_FIELD6")
  private String flexField6;

  @Column(name = "FLEX_FIELD_TYPE6")
  @Enumerated(EnumType.STRING)
  private FlexFieldType flexFieldType6;

  @Column(name = "FLEX_FIELD7")
  private String flexField7;

  @Column(name = "FLEX_FIELD_TYPE7")
  @Enumerated(EnumType.STRING)
  private FlexFieldType flexFieldType7;

  @Column(name = "FLEX_FIELD8")
  private String flexField8;

  @Column(name = "FLEX_FIELD_TYPE8")
  @Enumerated(EnumType.STRING)
  private FlexFieldType flexFieldType8;

  @Column(name = "FLEX_FIELD9")
  private String flexField9;

  @Column(name = "FLEX_FIELD_TYPE9")
  @Enumerated(EnumType.STRING)
  private FlexFieldType flexFieldType9;

  @Column(name = "FLEX_FIELD10")
  private String flexField10;

  @Column(name = "FLEX_FIELD_TYPE10")
  @Enumerated(EnumType.STRING)
  private FlexFieldType flexFieldType10;

  @Column(name = "FLEX_FIELD11")
  private String flexField11;

  @Column(name = "FLEX_FIELD_TYPE11")
  @Enumerated(EnumType.STRING)
  private FlexFieldType flexFieldType11;

  @Column(name = "FLEX_FIELD12")
  private String flexField12;

  @Column(name = "FLEX_FIELD_TYPE12")
  @Enumerated(EnumType.STRING)
  private FlexFieldType flexFieldType12;

  @Column(name = "FLEX_FIELD13")
  private String flexField13;

  @Column(name = "FLEX_FIELD_TYPE13")
  @Enumerated(EnumType.STRING)
  private FlexFieldType flexFieldType13;

  @Column(name = "FLEX_FIELD14")
  private String flexField14;

  @Column(name = "FLEX_FIELD_TYPE14")
  @Enumerated(EnumType.STRING)
  private FlexFieldType flexFieldType14;

  @Column(name = "FLEX_FIELD15")
  private String flexField15;

  @Column(name = "FLEX_FIELD_TYPE15")
  @Enumerated(EnumType.STRING)
  private FlexFieldType flexFieldType15;

  private ReconObject(ReconObjectBuilder builder) {
    this.objectStageId = builder.objectStageId;
    this.objectTypeId = builder.objectTypeId;
    this.message = builder.message;
    this.objectIdentifier = builder.objectIdentifier;
    this.parentIdentifier = builder.parentIdentifier;
    this.statusCode = builder.statusCode;
    this.objectVersion = builder.objectVersion;

    this.objectIdentifierType = builder.objectIdentifierType;
    this.parentIdentifierType = builder.parentIdentifierType;
    this.versionType = builder.versionType;

    this.flexField1 = builder.flexField1;
    this.flexField2 = builder.flexField2;
    this.flexField3 = builder.flexField3;
    this.flexField4 = builder.flexField4;
    this.flexField5 = builder.flexField5;
    this.flexField6 = builder.flexField6;
    this.flexField7 = builder.flexField7;
    this.flexField8 = builder.flexField8;
    this.flexField9 = builder.flexField9;
    this.flexField10 = builder.flexField10;
    this.flexField11 = builder.flexField11;
    this.flexField12 = builder.flexField12;
    this.flexField13 = builder.flexField13;
    this.flexField14 = builder.flexField14;
    this.flexField15 = builder.flexField15;

    this.flexFieldType1 = builder.flexFieldType1;
    this.flexFieldType2 = builder.flexFieldType2;
    this.flexFieldType3 = builder.flexFieldType3;
    this.flexFieldType4 = builder.flexFieldType4;
    this.flexFieldType5 = builder.flexFieldType5;
    this.flexFieldType6 = builder.flexFieldType6;
    this.flexFieldType7 = builder.flexFieldType7;
    this.flexFieldType8 = builder.flexFieldType8;
    this.flexFieldType9 = builder.flexFieldType9;
    this.flexFieldType10 = builder.flexFieldType10;
    this.flexFieldType11 = builder.flexFieldType11;
    this.flexFieldType12 = builder.flexFieldType12;
    this.flexFieldType13 = builder.flexFieldType13;
    this.flexFieldType14 = builder.flexFieldType14;
    this.flexFieldType15 = builder.flexFieldType15;
  }

  public boolean merge(ReconObject other) {
    if (this.objectVersion.compareTo(other.objectVersion) >= 0) {
      return false;
    }
    this.objectVersion = other.objectVersion;
    this.objectTypeId = other.objectTypeId;
    this.message = other.message;
    this.objectIdentifier = other.objectIdentifier;
    this.parentIdentifier = other.parentIdentifier;
    this.statusCode = RED;
    this.objectIdentifierType = other.objectIdentifierType;
    this.parentIdentifierType = other.parentIdentifierType;
    this.versionType = other.versionType;
    this.objectStageId = other.objectStageId;

    this.flexField1 = other.flexField1;
    this.flexField2 = other.flexField2;
    this.flexField3 = other.flexField3;
    this.flexField4 = other.flexField4;
    this.flexField5 = other.flexField5;
    this.flexField6 = other.flexField6;
    this.flexField7 = other.flexField7;
    this.flexField8 = other.flexField8;
    this.flexField9 = other.flexField9;
    this.flexField10 = other.flexField10;
    this.flexField11 = other.flexField11;
    this.flexField12 = other.flexField12;
    this.flexField13 = other.flexField13;
    this.flexField14 = other.flexField14;
    this.flexField15 = other.flexField15;

    this.flexFieldType1 = other.flexFieldType1;
    this.flexFieldType2 = other.flexFieldType2;
    this.flexFieldType3 = other.flexFieldType3;
    this.flexFieldType4 = other.flexFieldType4;
    this.flexFieldType5 = other.flexFieldType5;
    this.flexFieldType6 = other.flexFieldType6;
    this.flexFieldType7 = other.flexFieldType7;
    this.flexFieldType8 = other.flexFieldType8;
    this.flexFieldType9 = other.flexFieldType9;
    this.flexFieldType10 = other.flexFieldType10;
    this.flexFieldType11 = other.flexFieldType11;
    this.flexFieldType12 = other.flexFieldType12;
    this.flexFieldType13 = other.flexFieldType13;
    this.flexFieldType14 = other.flexFieldType14;
    this.flexFieldType15 = other.flexFieldType15;

    return true;
  }

  private static String getFlexFieldForEvaluation(String flexField, FlexFieldType type, boolean forEvaluation) {
    final Predicate<String> isTrueValue =
        p -> "Y".equalsIgnoreCase(p) || "true".equalsIgnoreCase(p) || "yes".equalsIgnoreCase(p);

    if (flexField == null) {
      return null;
    }
    if (!forEvaluation) {
      return flexField;
    }
    if (FlexFieldType.BOOLEAN.equals(type)) {
      return Boolean.toString(isTrueValue.test(flexField));
    }
    return flexField;
  }

  public String getFlexField(String flexField, boolean forEvaluation) {
    switch (flexField) {
      case OBJECT_IDENTIFIER:
        return getFlexFieldForEvaluation(objectIdentifier, objectIdentifierType, forEvaluation);
      case PARENT_IDENTIFIER:
        return getFlexFieldForEvaluation(parentIdentifier, parentIdentifierType, forEvaluation);
      case VERSION:
        return getFlexFieldForEvaluation(objectVersion, versionType, forEvaluation);
      case FLEX_FIELD1:
        return getFlexFieldForEvaluation(flexField1, flexFieldType1, forEvaluation);
      case FLEX_FIELD2:
        return getFlexFieldForEvaluation(flexField2, flexFieldType2, forEvaluation);
      case FLEX_FIELD3:
        return getFlexFieldForEvaluation(flexField3, flexFieldType3, forEvaluation);
      case FLEX_FIELD4:
        return getFlexFieldForEvaluation(flexField4, flexFieldType4, forEvaluation);
      case FLEX_FIELD5:
        return getFlexFieldForEvaluation(flexField5, flexFieldType5, forEvaluation);
      case FLEX_FIELD6:
        return getFlexFieldForEvaluation(flexField6, flexFieldType6, forEvaluation);
      case FLEX_FIELD7:
        return getFlexFieldForEvaluation(flexField7, flexFieldType7, forEvaluation);
      case FLEX_FIELD8:
        return getFlexFieldForEvaluation(flexField8, flexFieldType8, forEvaluation);
      case FLEX_FIELD9:
        return getFlexFieldForEvaluation(flexField9, flexFieldType9, forEvaluation);
      case FLEX_FIELD10:
        return getFlexFieldForEvaluation(flexField10, flexFieldType10, forEvaluation);
      case FLEX_FIELD11:
        return getFlexFieldForEvaluation(flexField11, flexFieldType11, forEvaluation);
      case FLEX_FIELD12:
        return getFlexFieldForEvaluation(flexField12, flexFieldType12, forEvaluation);
      case FLEX_FIELD13:
        return getFlexFieldForEvaluation(flexField13, flexFieldType13, forEvaluation);
      case FLEX_FIELD14:
        return getFlexFieldForEvaluation(flexField14, flexFieldType14, forEvaluation);
      case FLEX_FIELD15:
        return getFlexFieldForEvaluation(flexField15, flexFieldType15, forEvaluation);
      default:
        return null;

    }
  }

  public void updateStatus(List<RuleExecution> executions) {
    boolean status = true;
    for (RuleExecution e : executions) {
      if (RED.equals(e.getStatusCode())) {
        status = false;
        break;
      }
    }
    this.statusCode = StatusCode.from(status);
    log.info("ReconObject {} was set to {} status.", this.id, this.statusCode.toString());
  }

  public void statusNoRules() {
    this.statusCode = GREEN;
    this.statusMessage = "No rules found for this object";
  }

  public static class ReconObjectBuilder {

    public ReconObject build() {
      Validate.notNull(this.objectStageId, "Object Stage Id can not be null!");
      Validate.notNull(this.objectTypeId, "Object Type Id can not be null!");
      Validate.notBlank(this.message, "Message can not be blank!");
      Validate.notBlank(this.objectIdentifier, "Object Identifier can not be blank!");
      Validate.notNull(this.objectIdentifierType, "Object Identifier Type can not be null!");
      Validate.notBlank(this.objectVersion, "Version can not be blank!");
      Validate.notNull(this.versionType, "Version Type can not be null!");
      Validate.notNull(this.statusCode, "Status Code can not be null!");

      return new ReconObject(this);
    }
  }
}
