package com.plaszlo.apps.domain.model.definition.rule;

import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author plaszlo
 */
public interface RuleRepository extends PagingAndSortingRepository<Rule, Long> {

  List<Rule> findByObjectTypeId(Long objectTypeId);

  List<Rule> findByComparingObjectTypeId(Long comparingObjectTypeId);

  Optional<Rule> findByRuleNameIgnoreCaseAndObjectTypeId(String name, Long objectTypeId);

  List<Rule> findAll();

}
