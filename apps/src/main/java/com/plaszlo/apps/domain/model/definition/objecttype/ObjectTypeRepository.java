package com.plaszlo.apps.domain.model.definition.objecttype;

import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author plaszlo
 */
public interface ObjectTypeRepository extends PagingAndSortingRepository<ObjectType, Long> {

  Iterable<ObjectType> findAllByEnabledFlag(boolean enabledFlag);

  Optional<ObjectType> findByNameIgnoreCase(String name);

  List<ObjectType> findAll();
}
