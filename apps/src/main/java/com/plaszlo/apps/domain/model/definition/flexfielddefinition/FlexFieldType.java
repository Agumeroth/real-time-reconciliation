package com.plaszlo.apps.domain.model.definition.flexfielddefinition;

import java.io.Serializable;
import org.apache.commons.lang3.StringUtils;

/**
 * @author plaszlo
 */
public enum FlexFieldType implements Serializable {
  NUMBER, DATE, STRING, BOOLEAN;

  public boolean sameValueAs(FlexFieldType other) {
    return this.equals(other);
  }

  @Override
  public String toString() {
    return this.name();
  }

  public static FlexFieldType fromString(String value) {
    if (StringUtils.isBlank(value)) {
      return STRING;
    }
    for (FlexFieldType element : values()) {
      if (element.name().equalsIgnoreCase(value)) {
        return element;
      }
    }
    return null;
  }
}
