package com.plaszlo.apps.domain.model.definition.flexfielddefinition;

import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author plaszlo
 */
public interface FlexFieldDefinitionRepository extends PagingAndSortingRepository<FlexFieldDefinition, Long> {

  List<FlexFieldDefinition> findByObjectTypeId(Long objectTypeId);

  Optional<FlexFieldDefinition> findByObjectTypeIdAndColumnName(Long objectTypeId, String column);
}
