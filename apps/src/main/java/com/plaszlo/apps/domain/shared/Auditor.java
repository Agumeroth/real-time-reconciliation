package com.plaszlo.apps.domain.shared;

import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import lombok.Getter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

/**
 * @author plaszlo
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Getter
public abstract class Auditor {

  @Column(name = "CREATION_DATE")
  @CreationTimestamp
  private Instant creationDate;

  @Column(name = "LAST_UPDATE_DATE")
  @UpdateTimestamp
  private Instant lastUpdateDate;

}
