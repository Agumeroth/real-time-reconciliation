package com.plaszlo.apps.domain.model.execution.ruleexecution;

import com.plaszlo.apps.application.execution.ruleevaluation.SharedRuleEvaluationService.EvaluationResult;
import com.plaszlo.apps.domain.model.execution.reconobject.StatusCode;
import com.plaszlo.apps.domain.shared.Auditor;
import com.plaszlo.apps.infrastructure.persistence.ListToStringConverter;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang3.Validate;

/**
 * @author plaszlo
 */
@Entity
@Table(name = "REC_RULE_EXECUTION")
@ToString
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RuleExecution extends Auditor {

  @Id
  @Column(name = "RULE_EXECUTION_ID", nullable = false)
  @SequenceGenerator(name = "REC_SEQ_GENERATOR", sequenceName = "REC_SEQ", allocationSize = 100)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REC_SEQ_GENERATOR")
  private Long id;

  @Column(name = "OBJECT_ID", nullable = false)
  private Long objectId;

  @Column(name = "COMPARING_OBJECT_ID")
  @Convert(converter = ListToStringConverter.class)
  @Getter
  private List<Long> comparingObjectId;

  @Column(name = "RULE_ID", nullable = false)
  private Long ruleId;

  @Column(name = "EVALUATED_RULE", nullable = false)
  private String evaluatedRule;

  @Column(name = "STATUS_CODE", nullable = false)
  @Enumerated(EnumType.STRING)
  @Getter
  private StatusCode statusCode;

  @Column(name = "STATUS_MESSAGE")
  private String statusMessage;

  private RuleExecution(RuleExecutionBuilder builder) {
    this.objectId = builder.objectId;
    this.comparingObjectId = builder.comparingObjectId;
    this.ruleId = builder.ruleId;
    this.evaluatedRule = builder.evaluatedRule;
    this.statusCode = builder.statusCode;
    this.statusMessage = builder.statusMessage;
  }

  public void updateAttributes(EvaluationResult evaluationResult) {
    this.comparingObjectId = evaluationResult.getComparingObjectIds();
    this.evaluatedRule = evaluationResult.getEvaluatedRule();
    this.statusCode = StatusCode.from(evaluationResult.getResult());
    this.statusMessage = null;
  }

  public static class RuleExecutionBuilder {

    public RuleExecution build() {
      Validate.notNull(this.objectId, "Object Id can not be null!");
      Validate.notNull(this.ruleId, "Rule Id can not be null!");
      Validate.notBlank(this.evaluatedRule, "Rule Definition can not be blank!");
      Validate.notNull(this.statusCode, "Status Code can not be null!");

      return new RuleExecution(this);
    }
  }
}
