package com.plaszlo.apps.domain.model.definition.rule;

import static java.lang.Boolean.FALSE;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.plaszlo.apps.domain.shared.Auditor;
import com.plaszlo.apps.infrastructure.messaging.BooleanDeserializer;
import com.plaszlo.apps.infrastructure.persistence.BooleanToStringConverter;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.jdbc.datasource.init.ScriptParseException;
import org.springframework.scripting.ScriptCompilationException;

/**
 * @author plaszlo
 */
@Entity
@Table(name = "REC_RULE")
@Getter
@ToString
@Builder
@Slf4j
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Rule extends Auditor {

  private static final String JS_ENGINE = "javascript";

  @Id
  @Column(name = "RULE_ID", nullable = false)
  @SequenceGenerator(name = "REC_SEQ_GENERATOR", sequenceName = "REC_SEQ", allocationSize = 100)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REC_SEQ_GENERATOR")
  private Long id;

  @Column(name = "RULE_NAME", nullable = false)
  private String ruleName;

  @Column(name = "OBJECT_TYPE_ID", nullable = false)
  private Long objectTypeId;

  @Column(name = "COMPARING_OBJECT_TYPE_ID", nullable = false)
  private Long comparingObjectTypeId;

  @Column(name = "ONE_TO_MANY_FLAG", nullable = false, length = 1)
  @Convert(converter = BooleanToStringConverter.class)
  @JsonDeserialize(using = BooleanDeserializer.class)
  private boolean oneToManyFlag;

  @Column(name = "MATCHING_RULE_DEFINITION", nullable = false)
  private String matchingRuleDefinition;

  @Column(name = "RULE_DEFINITION", nullable = false)
  private String ruleDefinition;

  private Rule(RuleBuilder builder) {
    this.ruleName = builder.ruleName;
    this.objectTypeId = builder.objectTypeId;
    this.comparingObjectTypeId = builder.comparingObjectTypeId;
    this.oneToManyFlag = builder.oneToManyFlag;
    this.matchingRuleDefinition = builder.matchingRuleDefinition;
    this.ruleDefinition = builder.ruleDefinition;
  }

  public void merge(Rule other) {
    this.ruleName = other.getRuleName();
    this.comparingObjectTypeId = other.getComparingObjectTypeId();
    this.oneToManyFlag = other.isOneToManyFlag();
    this.matchingRuleDefinition = other.getMatchingRuleDefinition();
    this.ruleDefinition = other.getRuleDefinition();
  }

  public Boolean evaluateRule(String rule, Map<String, Object> valueMap) {
    ScriptEngine engine = initializeScriptEngine(valueMap);
    log.debug("Initialized javascript engine, starting rule evaluation for rule: {}", rule);
    try {
      engine.eval("load('classpath:Functions.js')");
      return Boolean.parseBoolean(engine.eval(rule).toString());
    } catch (ScriptException | ScriptParseException | ScriptCompilationException ex) {
      log.error("Could not evaluate rule: {}, exception: {}", rule, ex.getCause().toString());
      return FALSE;
    } catch (Exception e) {
      log.error("Exception occurred during evaluation: {}", e.getCause().toString());
      return FALSE;
    }
  }

  private ScriptEngine initializeScriptEngine(Map<String, Object> valueMap) {
    ScriptEngineManager manager = new ScriptEngineManager();
    ScriptEngine engine = manager.getEngineByName(JS_ENGINE);

    valueMap.forEach((k, v) -> {
      if (StringUtils.isNotBlank(v.toString())) {
        engine.put(k, v);
      }
    });
    return engine;
  }


  public static class RuleBuilder {

    public Rule build() {
      Validate.notBlank(this.ruleName, "Rule name can not be blank!");
      Validate.notNull(this.objectTypeId, "Object Type Id can not be null!");
      Validate.notNull(this.comparingObjectTypeId, "Comparing Object Type Id can not be null!");
      Validate.notBlank(this.matchingRuleDefinition, "Matching Rule Definition can not be blank!");
      Validate.notBlank(this.ruleDefinition, "Rule Definition can not be blank!");

      return new Rule(this);
    }
  }

}
