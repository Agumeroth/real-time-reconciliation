package com.plaszlo.apps.domain.model.definition.flexfielddefinition;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.plaszlo.apps.application.definition.flexfielddefinition.FlexFieldTypeSerializer;
import com.plaszlo.apps.application.definition.setup.FlexFieldTypeDeserializer;
import com.plaszlo.apps.domain.shared.Auditor;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;

/**
 * @author plaszlo
 */
@Entity
@Table(name = "REC_FLEX_FIELD_DEFINITION")
@Builder
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class FlexFieldDefinition extends Auditor {

  @Id
  @Column(name = "FLEX_FIELD_DEFINITION_ID", nullable = false)
  @SequenceGenerator(name = "REC_SEQ_GENERATOR", sequenceName = "REC_SEQ", allocationSize = 100)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REC_SEQ_GENERATOR")
  private Long id;

  @Column(name = "OBJECT_TYPE_ID", nullable = false)
  @Setter
  private Long objectTypeId;

  @Column(name = "OBJECT_ATTRIBUTE_NAME", nullable = false)
  private String objectAttributeName;

  @Column(name = "COLUMN_NAME", nullable = false)
  private String columnName;

  @Column(name = "COLUMN_TYPE")
  @Enumerated(EnumType.STRING)
  @JsonDeserialize(using = FlexFieldTypeDeserializer.class)
  @JsonSerialize(using = FlexFieldTypeSerializer.class)
  private FlexFieldType columnType;

  @Column(name = "DERIVATION_LOGIC", nullable = false)
  private String derivationLogic;

  public void merge(FlexFieldDefinition other) {
    this.objectAttributeName = other.getObjectAttributeName();
    this.columnName = other.getColumnName();
    this.columnType = ObjectUtils.defaultIfNull(other.getColumnType(), FlexFieldType.STRING);
    this.derivationLogic = other.getDerivationLogic();
  }

  private FlexFieldDefinition(FlexFieldDefinitionBuilder builder) {
    this.objectTypeId = builder.objectTypeId;
    this.objectAttributeName = builder.objectAttributeName;
    this.columnName = builder.columnName;
    this.columnType = builder.columnType;
    this.derivationLogic = builder.derivationLogic;
  }

  @Override
  public String toString() {
    return "FlexFieldDefinition(id=" + this.id + ", objectTypeId=" + this.objectTypeId + ", objectAttributeName="
        + this.objectAttributeName + ", columnName=" + this.columnName + ", columnType=" + this.columnType
        + ", derivationLogic=" + this.derivationLogic + ")";
  }

  public boolean isValidForCreation() {
    return StringUtils.isNotBlank(this.getObjectAttributeName())
        && StringUtils.isNotBlank(this.getColumnName())
        && StringUtils.isNotBlank(this.getDerivationLogic());
  }

  public static class FlexFieldDefinitionBuilder {

    public FlexFieldDefinition build() {
      Validate.notNull(this.objectTypeId, "ObjectTypeId can not be null!");
      Validate.notBlank(this.objectAttributeName, "ObjectAttributeName can not be blank!");
      Validate.notBlank(this.columnName, "ColumnName can not be blank!");
      Validate.notNull(this.columnType, "ColumnType can not be null!");
      Validate.notBlank(this.derivationLogic, "DerivationLogic can not be blank!");

      return new FlexFieldDefinition(this);
    }

  }
}
