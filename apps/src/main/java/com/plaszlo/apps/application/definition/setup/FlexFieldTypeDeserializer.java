package com.plaszlo.apps.application.definition.setup;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.plaszlo.apps.domain.model.definition.flexfielddefinition.FlexFieldType;
import java.io.IOException;

/**
 * @author plaszlo
 */
public class FlexFieldTypeDeserializer extends JsonDeserializer<FlexFieldType> {

  @Override
  public FlexFieldType deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
      throws IOException {
    return FlexFieldType.fromString(jsonParser.getText());
  }
}
