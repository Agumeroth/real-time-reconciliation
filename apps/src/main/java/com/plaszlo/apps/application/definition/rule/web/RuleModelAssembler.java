package com.plaszlo.apps.application.definition.rule.web;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.plaszlo.apps.domain.model.definition.rule.Rule;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

@Component
public class RuleModelAssembler implements RepresentationModelAssembler<Rule, EntityModel<Rule>> {

  @Override
  public EntityModel<Rule> toModel(Rule entity) {
    return EntityModel.of(entity,
        WebMvcLinkBuilder.linkTo(methodOn(RuleController.class).oneRule(entity.getObjectTypeId(),
            entity.getId())).withSelfRel(),
        linkTo(methodOn(RuleController.class).allRules(entity.getObjectTypeId())).withRel("rules"));
  }
}
