package com.plaszlo.apps.application.definition.flexfielddefinition;

import com.plaszlo.apps.domain.model.definition.flexfielddefinition.FlexFieldDefinition;
import com.plaszlo.apps.domain.model.definition.objecttype.ObjectType;
import java.util.Optional;

/**
 * @author plaszlo
 */
public interface FlexFieldDefinitionService {

  /**
   * @param flexFieldDefinition
   * @param objectType
   * @return
   */
  Optional<FlexFieldDefinition> handleFlexFieldDefinitionCreation(ObjectType objectType,
      FlexFieldDefinition flexFieldDefinition);
}
