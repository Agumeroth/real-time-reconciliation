package com.plaszlo.apps.application.definition.objecttype.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ObjectTypeNotFoundAdvice {

  @ResponseBody
  @ExceptionHandler(ObjectTypeNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  String objectTypeNotFoundHandler(ObjectTypeNotFoundException e) {
    return e.getMessage();
  }
}
