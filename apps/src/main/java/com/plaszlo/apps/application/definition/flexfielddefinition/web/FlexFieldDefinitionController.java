package com.plaszlo.apps.application.definition.flexfielddefinition.web;

import static com.plaszlo.apps.application.execution.reconobjectstage.reprocess.ReconObjectStageReprocessService.createResponse;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.plaszlo.apps.application.definition.flexfielddefinition.FlexFieldDefinitionService;
import com.plaszlo.apps.application.definition.objecttype.web.ObjectTypeNotFoundException;
import com.plaszlo.apps.domain.model.definition.flexfielddefinition.FlexFieldDefinition;
import com.plaszlo.apps.domain.model.definition.flexfielddefinition.FlexFieldDefinitionRepository;
import com.plaszlo.apps.domain.model.definition.objecttype.ObjectType;
import com.plaszlo.apps.domain.model.definition.objecttype.ObjectTypeRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author plaszlo
 */
@RestController
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class FlexFieldDefinitionController {

  private final ObjectTypeRepository objectTypeRepository;
  private final FlexFieldDefinitionModelAssembler flexFieldDefinitionModelAssembler;
  private final FlexFieldDefinitionRepository flexFieldDefinitionRepository;
  private final FlexFieldDefinitionService service;

  @PostMapping(path = "/objectType/{objectTypeId}/flexFieldDefinitions")
  public ResponseEntity<String> setupFlexFieldDefinition(@PathVariable Long objectTypeId,
      @RequestBody List<FlexFieldDefinition> json) {
    if (json.isEmpty()) {
      createResponse("Not given any FlexFieldDefinitions to create!", HttpStatus.BAD_REQUEST);
    }
    List<FlexFieldDefinition> errorList = new ArrayList<>(json.size());
    List<FlexFieldDefinition> createdList = new ArrayList<>(json.size());
    List<FlexFieldDefinition> existingList = new ArrayList<>(json.size());

    json.forEach(e -> {
      if (!e.isValidForCreation()) {
        errorList.add(e);
        return;
      }
      ObjectType objectType = objectTypeRepository.findById(objectTypeId).orElseThrow(() ->
          new ObjectTypeNotFoundException(objectTypeId));
      Optional<FlexFieldDefinition> createdDefinition = service.handleFlexFieldDefinitionCreation(objectType, e);

      if (createdDefinition.isPresent()) {
        createdList.add(createdDefinition.get());
      } else {
        existingList.add(e);
      }
    });
    if (!errorList.isEmpty()) {
      return createResponse("Given attributes are not valid for creation for the following "
          + "FlexFieldDefinitions: " + errorList.toString(), HttpStatus.BAD_REQUEST);
    }

    if (createdList.size() == json.size() && existingList.isEmpty()) {
      flexFieldDefinitionRepository.saveAll(createdList);
      return createResponse("FlexFieldDefinitions created: " + createdList.toString(), HttpStatus.OK);
    }
    return createResponse("FlexFieldDefinitions " + existingList.toString() + " already exist!",
        HttpStatus.BAD_REQUEST);
  }

  @GetMapping(path = "/objectType/{objectTypeId}/flexFieldDefinitions")
  public CollectionModel<EntityModel<FlexFieldDefinition>> allFlexFieldDefinitions(@PathVariable Long objectTypeId) {
    ObjectType objectType = objectTypeRepository.findById(objectTypeId).orElseThrow(() ->
        new ObjectTypeNotFoundException(objectTypeId));
    List<EntityModel<FlexFieldDefinition>> flexFieldDefinitions =
        flexFieldDefinitionRepository.findByObjectTypeId(objectType.getId())
            .stream()
            .map(flexFieldDefinitionModelAssembler::toModel)
            .collect(Collectors.toList());
    return CollectionModel.of(flexFieldDefinitions,
        linkTo(methodOn(FlexFieldDefinitionController.class).allFlexFieldDefinitions(objectTypeId)).withSelfRel());
  }

  @PutMapping(path = "/objectType/{objectTypeId}/flexFieldDefinition/{ffId}")
  public ResponseEntity<String> updateRule(@PathVariable Long objectTypeId, @PathVariable Long ffId,
      @RequestBody FlexFieldDefinition json) {
    if (!json.isValidForCreation()) {
      return createResponse("Given FlexFieldDefinition attributes are not valid for creation: "
          + json.toString(), HttpStatus.BAD_REQUEST);
    }
    ObjectType objectType = objectTypeRepository.findById(objectTypeId).orElseThrow(() ->
        new ObjectTypeNotFoundException(objectTypeId));
    FlexFieldDefinition flexFieldDefinition = flexFieldDefinitionRepository.findById(ffId).orElseThrow(() ->
        new FlexFieldDefinitionNotFoundException(ffId));
    if (!flexFieldDefinition.getObjectTypeId().equals(objectType.getId())) {
      throw new FlexFieldDefinitionNotFoundException(objectTypeId, ffId);
    }

    flexFieldDefinition.merge(json);
    flexFieldDefinitionRepository.save(flexFieldDefinition);
    return createResponse("FlexFieldDefinition updated: " + flexFieldDefinition.toString(), HttpStatus.OK);
  }

  @GetMapping(path = "/objectType/{objectTypeId}/flexFieldDefinition/{ffId}")
  public EntityModel<FlexFieldDefinition> oneFlexFieldDefinition(@PathVariable Long objectTypeId,
      @PathVariable Long ffId) {
    ObjectType objectType = objectTypeRepository.findById(objectTypeId).orElseThrow(() ->
        new ObjectTypeNotFoundException(objectTypeId));
    FlexFieldDefinition flexFieldDefinition = flexFieldDefinitionRepository.findById(ffId)
        .orElseThrow(() -> new FlexFieldDefinitionNotFoundException(ffId));
    if (!flexFieldDefinition.getObjectTypeId().equals(objectType.getId())) {
      throw new FlexFieldDefinitionNotFoundException(objectTypeId, ffId);
    }
    return flexFieldDefinitionModelAssembler.toModel(flexFieldDefinition);
  }
}
