package com.plaszlo.apps.application.execution.ruleevaluation;

import static com.plaszlo.apps.application.execution.ruleevaluation.RuleEvaluationServiceImpl.createNewExecution;

import com.plaszlo.apps.domain.model.definition.flexfielddefinition.FlexFieldDefinitionRepository;
import com.plaszlo.apps.domain.model.definition.objecttype.ObjectTypeRepository;
import com.plaszlo.apps.domain.model.definition.rule.Rule;
import com.plaszlo.apps.domain.model.execution.reconobject.ReconObject;
import com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectRepository;
import com.plaszlo.apps.domain.model.execution.ruleexecution.RuleExecution;
import com.plaszlo.apps.domain.model.execution.ruleexecution.RuleExecutionRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author plaszlo
 */
@Service
@Slf4j
public class OneToOneService extends SharedRuleEvaluationService {

  private final RuleExecutionRepository ruleExecutionRepository;

  public OneToOneService(
      ReconObjectRepository objectRepository,
      ObjectTypeRepository objectTypeRepository,
      FlexFieldDefinitionRepository flexFieldDefinitionRepository,
      RuleExecutionRepository ruleExecutionRepository) {
    super(objectRepository, objectTypeRepository, flexFieldDefinitionRepository, ruleExecutionRepository);
    this.ruleExecutionRepository = ruleExecutionRepository;
  }

  public List<RuleExecution> createOrUpdateRuleExecutions(ReconObject object, Rule rule) {
    log.info("One to One type Rule {} found for Object Id {}", rule.getRuleName(), object.getId());
    List<RuleExecution> executions = evaluateRuleForObject(rule, object);
    if (!executions.isEmpty()) {
      ruleExecutionRepository.saveAll(executions);
      updateObjectStatus(object);
    }
    return executions;
  }

  private List<RuleExecution> evaluateRuleForObject(Rule rule, ReconObject object) {
    List<RuleExecution> resultList = new ArrayList<>();
    List<ReconObject> comparableObjects = this.findComparableObjects(rule.getComparingObjectTypeId());
    Map<String, Object> objectValueMap = this.createMap(object);

    if (objectValueMap.isEmpty() || comparableObjects.isEmpty()) {
      log.debug("Value map or comparable Objects could not be found for Object Id {} with Object Type Id {}",
          object.getId(), object.getObjectTypeId());
      return resultList;
    }

    String ruleDefinition = this.refineDefinition(rule.getRuleDefinition());
    String matchingRule = this.refineDefinition(rule.getMatchingRuleDefinition());

    comparableObjects.forEach(o -> {
      Map<String, Object> comparingObjectMap = this.createMap(o);
      if (comparingObjectMap.isEmpty()) {
        log.debug("Comparing Object value map not found for Object Id {} with Object Type Id {}",
            o.getId(), o.getObjectTypeId());
        return;
      }
      Map<String, Object> completeMap = this.mergeMap(objectValueMap, comparingObjectMap);
      boolean isMatching = rule.evaluateRule(matchingRule, completeMap);

      if (isMatching) {
        log.debug("Found matching Object with Id {} for Object Id {}", object.getId(), o.getId());
        Boolean result = rule.evaluateRule(ruleDefinition, completeMap);
        String evaluatedRule = this.createEvaluatedRule(ruleDefinition, completeMap);
        List<Long> comparingObjectIds = Arrays.asList(o.getId());
        log.info("Creating RuleExecution for Object Id {} and Rule {} with comparing Object Id {}",
            object.getId(), rule.getRuleName(), o.getId());
        EvaluationResult evaluationResult = new EvaluationResult(comparingObjectIds, result, evaluatedRule);

        Optional<RuleExecution> existingExecutionOp =
            findExistingRuleExecution(object.getId(), rule.getId(), o.getId());
        if (existingExecutionOp.isPresent()) {
          existingExecutionOp.get().updateAttributes(evaluationResult);
          resultList.add(existingExecutionOp.get());
          return;
        }
        Optional<RuleExecution> placeholderOp = findPlaceholderExecution(object.getId(), rule.getId());
        if (placeholderOp.isPresent()) {
          RuleExecution placeholder = placeholderOp.get();
          placeholder.updateAttributes(evaluationResult);
          resultList.add(placeholder);
        } else {
          RuleExecution execution = createNewExecution(object, rule, evaluationResult).build();
          resultList.add(execution);
        }
      }
    });
    return resultList;
  }

  private Optional<RuleExecution> findExistingRuleExecution(Long objectId, Long ruleId, Long comparingObjectId) {
    List<RuleExecution> executionsForObject = ruleExecutionRepository.findByRuleIdAndObjectId(ruleId, objectId);

    for (RuleExecution ex : executionsForObject) {
      if (ex.getComparingObjectId().contains(comparingObjectId)) {
        return Optional.of(ex);
      }
    }
    return Optional.empty();
  }

  private Optional<RuleExecution> findPlaceholderExecution(Long objectId, Long ruleId) {
    return ruleExecutionRepository.findPlaceholderExecution(ruleId, objectId);
  }
}
