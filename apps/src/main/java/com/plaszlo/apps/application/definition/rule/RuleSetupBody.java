package com.plaszlo.apps.application.definition.rule;

import com.plaszlo.apps.domain.model.definition.rule.Rule;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

public class RuleSetupBody extends Rule {

  @Getter
  private String comparingObjectTypeName;


  public boolean isValidForCreation() {
    return StringUtils.isNotBlank(this.comparingObjectTypeName)
        && StringUtils.isNotBlank(this.getRuleName())
        && StringUtils.isNotBlank(this.getRuleDefinition())
        && StringUtils.isNotBlank(this.getMatchingRuleDefinition());
  }


}
