package com.plaszlo.apps.application.shared;

import static java.lang.Boolean.TRUE;

import java.util.HashMap;
import java.util.Map;
import javax.persistence.OptimisticLockException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.orm.jpa.JpaOptimisticLockingFailureException;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

/**
 * @author plaszlo
 */
@Configuration
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RetryConfiguration {

  private final Environment environment;

  @Bean
  public RetryTemplate retryTemplate() {
    RetryTemplate retryTemplate = new RetryTemplate();

    long initialInterval = environment.getRequiredProperty("oalcn-rtrecon.retry.initialInterval", Long.class);
    long maxInterval = environment.getRequiredProperty("oalcn-rtrecon.retry.maxInterval", Long.class);
    double multiplier = environment.getRequiredProperty("oalcn-rtrecon.retry.multiplier", Double.class);
    int maxAttempts = environment.getRequiredProperty("oalcn-rtrecon.retry.maxAttempts", Integer.class);

    retryTemplate.registerListener(new DefaultListenerSupport());
    retryTemplate.setThrowLastExceptionOnExhausted(true);

    ExponentialBackOffPolicy policy = new ExponentialBackOffPolicy();
    policy.setInitialInterval(initialInterval);
    policy.setMultiplier(multiplier);
    policy.setMaxInterval(maxInterval);
    retryTemplate.setBackOffPolicy(policy);

    Map<Class<? extends Throwable>, Boolean> exceptions = new HashMap<>();
    exceptions.put(OptimisticLockException.class, TRUE);
    exceptions.put(JpaOptimisticLockingFailureException.class, TRUE);
    exceptions.put(JpaSystemException.class, TRUE);

    SimpleRetryPolicy simpleRetryPolicy = new SimpleRetryPolicy(maxAttempts, exceptions);
    retryTemplate.setRetryPolicy(simpleRetryPolicy);

    return retryTemplate;
  }
}
