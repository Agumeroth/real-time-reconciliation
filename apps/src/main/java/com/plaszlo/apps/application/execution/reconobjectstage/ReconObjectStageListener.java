package com.plaszlo.apps.application.execution.reconobjectstage;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Component;

/**
 * @author plaszlo
 */
@Component
@Slf4j
@ConditionalOnProperty(prefix = "oalcn-rtrecon", value = "reconObjectStage.messaging.consumer.enabled",
    havingValue = "true")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ReconObjectStageListener {

  private final ReconObjectStageService service;
  private final RetryTemplate retryTemplate;

  @KafkaListener(id = "ReconObjectStageListener", groupId = "#{'${spring.kafka.consumer.group-id}'}",
      topics = "#{'${oalcn-rtrecon.reconObjectStage.messaging.consumer.topic}'.split(',')}")
  public void consumeObjectExtractedEvent(ConsumerRecord<String, ?> record) {
    retryTemplate.execute(c -> {
      service.handle(record);
      return null;
    });
  }

}
