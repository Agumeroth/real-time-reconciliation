package com.plaszlo.apps.application.execution.reconobjectstage.reprocess;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

import com.plaszlo.apps.application.execution.reconobjectstage.ReconObjectStagePersistedEvent;
import com.plaszlo.apps.domain.model.execution.reconobject.ReconObject;
import com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectRepository;
import com.plaszlo.apps.domain.model.execution.reconobjectstage.ReconObjectStage;
import com.plaszlo.apps.domain.model.execution.reconobjectstage.ReconObjectStageRepository;
import com.plaszlo.apps.domain.model.execution.ruleexecution.RuleExecution;
import com.plaszlo.apps.domain.model.execution.ruleexecution.RuleExecutionRepository;
import com.plaszlo.apps.domain.shared.DomainEventPublisher;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

/**
 * @author plaszlo
 */
@Component
@Slf4j
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ReconObjectStageReprocessService {

  private static final String NOT_TO_REPROCESS = "Reprocess was not selected";
  private static final String NOT_VALID_REQUEST = "Can not purge only ReconObjects without purging RuleExecutions!";
  private static final String REPROCESSED_ALL = "Started to reprocess all the stage objects";
  private static final String NO_STAGES_SELECTED = "No stages selected for reprocess";

  private final ReconObjectStageRepository stageRepository;
  private final ReconObjectRepository objectRepository;
  private final RuleExecutionRepository executionRepository;
  private final TransactionTemplate transactionTemplate;

  public ResponseEntity<String> processRequest(ReconObjectStageReprocessBody request) {
    if (!request.isPurgeObjects() && !request.isPurgeExecutions()) {
      return createResponse(NOT_TO_REPROCESS, OK);
    }

    if (request.isPurgeObjects() && !request.isPurgeExecutions()) {
      return createResponse(NOT_VALID_REQUEST, BAD_REQUEST);
    }

    if (StringUtils.isBlank(request.getTopic()) && request.getStartDate() == null) {
      return reprocessAll(request);
    }

    if (StringUtils.isNotBlank(request.getTopic())) {
      return reprocessWithTopic(request);
    }

    if (request.getStartDate() != null) {
      return reprocessBetweenDates(request);
    }

    return createResponse(NO_STAGES_SELECTED, OK);
  }

  private ResponseEntity<String> reprocessAll(ReconObjectStageReprocessBody request) {
    log.info("Reprocessing all ReconObjectStages");
    List<ReconObjectStage> stages = stageRepository.findAll();

    this.reprocess(stages, request);
    return createResponse(REPROCESSED_ALL, OK);
  }

  private ResponseEntity<String> reprocessWithTopic(ReconObjectStageReprocessBody request) {
    log.info("Reprocessing ReconObjectStages created from topic: {}", request.getTopic());

    List<ReconObjectStage> stages = stageRepository.findAllByTopic(request.getTopic());
    if (request.getStartDate() != null) {
      Instant endDate = request.getEndDate() != null ? request.getEndDate() : Instant.now();
      stages = stages.stream()
          .filter(s -> s.getCreationDate().isAfter(request.getStartDate()) && s.getCreationDate().isBefore(endDate))
          .collect(Collectors.toList());
    }
    if (reprocess(stages, request)) {
      return createResponse("Reprocess started for stages with topic: " + request.getTopic(), OK);
    }
    return createResponse("No stages found with topic: " + request.getTopic(), OK);
  }

  private ResponseEntity<String> reprocessBetweenDates(ReconObjectStageReprocessBody request) {
    Instant startDate = request.getStartDate();
    Instant endDate = request.getEndDate() != null ? request.getEndDate() : Instant.now();
    log.info("Reprocessing ReconObjectStages created between {} and {}", startDate, endDate);

    List<ReconObjectStage> stages = stageRepository.findAllBetween(startDate, endDate);

    if (reprocess(stages, request)) {
      return createResponse("Started reprocessing stage objects with creation date between "
          + Timestamp.from(startDate).toString() + " and " + Timestamp.from(endDate).toString(), OK);
    }
    return createResponse("No stage objects were found with creation date between "
        + Timestamp.from(startDate).toString() + " and " + Timestamp.from(endDate).toString(), OK);
  }

  public static ResponseEntity<String> createResponse(String message, HttpStatus status) {
    return new ResponseEntity<>(message, status);
  }

  private boolean reprocess(List<ReconObjectStage> list, ReconObjectStageReprocessBody request) {
    if (!list.isEmpty()) {
      log.info("ReconObjectStages to reprocess: {}", list.toString());
      list.forEach(s -> {
        List<ReconObject> objects = objectRepository.findByObjectStageId(s.getId());
        if (request.isPurgeObjects()) {
          this.deleteObjects(s);
        }
        this.deleteExecutions(objects);
        DomainEventPublisher.publish(ReconObjectStagePersistedEvent.of(s.getId(), s));
      });
      return true;
    }
    return false;
  }

  private void deleteObjects(ReconObjectStage stage) {
    transactionTemplate.execute(new TransactionCallbackWithoutResult() {
      @Override
      protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
        List<ReconObject> deletedObjects = objectRepository.deleteByObjectStageId(stage.getId());
        if (!deletedObjects.isEmpty()) {
          log.info("Deleted {} ReconObjects for stage id {}", deletedObjects.size(), stage.getId());
          return;
        }
        log.info("No Objects were deleted for stage id {}", stage.getId());
      }
    });
  }


  private void deleteExecutions(List<ReconObject> objects) {
    transactionTemplate.execute(new TransactionCallbackWithoutResult() {
      @Override
      protected void doInTransactionWithoutResult(TransactionStatus transactionStatus) {
        List<RuleExecution> deletedExecutions = new ArrayList<>();
        objects.forEach(o -> deletedExecutions.addAll(executionRepository.deleteByObjectId(o.getId())));
        if (deletedExecutions.isEmpty()) {
          log.info("No RuleExecutions found for selected Objects");
          return;
        }
        log.info("{} RuleExecutions deleted for reprocessed Objects", deletedExecutions.size());
      }
    });
  }
}
