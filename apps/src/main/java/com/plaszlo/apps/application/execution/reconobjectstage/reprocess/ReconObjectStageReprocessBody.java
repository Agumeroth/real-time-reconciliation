package com.plaszlo.apps.application.execution.reconobjectstage.reprocess;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.plaszlo.apps.infrastructure.messaging.TimeStampToInstantDeSerializer;
import java.time.Instant;
import lombok.Getter;
import lombok.Setter;

/**
 * @author plaszlo
 */
@Getter
@Setter
public class ReconObjectStageReprocessBody {

  private boolean purgeObjects;
  private boolean purgeExecutions;
  private String topic;
  @JsonDeserialize(using = TimeStampToInstantDeSerializer.class)
  private Instant startDate;
  @JsonDeserialize(using = TimeStampToInstantDeSerializer.class)
  private Instant endDate;
}
