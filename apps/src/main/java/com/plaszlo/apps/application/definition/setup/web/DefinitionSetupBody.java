package com.plaszlo.apps.application.definition.setup.web;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.plaszlo.apps.application.definition.rule.RuleSetupBody;
import com.plaszlo.apps.domain.model.definition.flexfielddefinition.FlexFieldDefinition;
import com.plaszlo.apps.domain.model.definition.objecttype.ObjectType;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class DefinitionSetupBody {

  private ObjectType objectType;

  private List<FlexFieldDefinition> flexFieldDefinitions;

  private List<RuleSetupBody> rules;

}
