package com.plaszlo.apps.application.definition.flexfielddefinition.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class FlexFieldDefinitionNotFoundAdvice {

  @ResponseBody
  @ExceptionHandler(FlexFieldDefinitionNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  String flexFieldDefinitionNotFoundHandler(FlexFieldDefinitionNotFoundException e) {
    return e.getMessage();
  }
}
