package com.plaszlo.apps.application.definition.rule.web;

import static com.plaszlo.apps.application.definition.rule.RuleServiceImpl.createRule;
import static com.plaszlo.apps.application.execution.reconobjectstage.reprocess.ReconObjectStageReprocessService.createResponse;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.plaszlo.apps.application.definition.objecttype.web.ObjectTypeNotFoundException;
import com.plaszlo.apps.application.definition.rule.RuleService;
import com.plaszlo.apps.application.definition.rule.RuleSetupBody;
import com.plaszlo.apps.domain.model.definition.objecttype.ObjectType;
import com.plaszlo.apps.domain.model.definition.objecttype.ObjectTypeRepository;
import com.plaszlo.apps.domain.model.definition.rule.Rule;
import com.plaszlo.apps.domain.model.definition.rule.RuleRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author plaszlo
 */
@RestController
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class RuleController {

  private final ObjectTypeRepository objectTypeRepository;
  private final RuleService service;
  private final RuleModelAssembler ruleModelAssembler;
  private final RuleRepository ruleRepository;

  @PostMapping(path = "/objectType/{objectTypeId}/rules")
  public ResponseEntity<String> setupRule(@PathVariable Long objectTypeId, @RequestBody List<RuleSetupBody> json) {
    if (json.isEmpty()) {
      createResponse("Not given any Rules to create!", HttpStatus.BAD_REQUEST);
    }
    List<RuleSetupBody> errorList = new ArrayList<>(json.size());
    List<Rule> createdList = new ArrayList<>(json.size());
    List<RuleSetupBody> existingList = new ArrayList<>(json.size());

    json.forEach(e -> {
      if (!e.isValidForCreation()) {
        errorList.add(e);
        return;
      }
      ObjectType objectType = objectTypeRepository.findById(objectTypeId).orElseThrow(() ->
          new ObjectTypeNotFoundException(objectTypeId));
      Optional<Rule> createdRule = service.handleRuleCreation(objectType, e);
      if (createdRule.isPresent()) {
        createdList.add(createdRule.get());
      } else {
        existingList.add(e);
      }
    });
    if (!errorList.isEmpty()) {
      return createResponse("Given attributes are not valid for creation for the following Rules: "
          + errorList.toString(), HttpStatus.BAD_REQUEST);
    }

    if (createdList.size() == json.size() && existingList.isEmpty()) {
      ruleRepository.saveAll(createdList);
      return createResponse("Rules created: " + createdList.toString(), HttpStatus.OK);
    }
    return createResponse("Rules " + existingList.toString() + " already exist!", HttpStatus.BAD_REQUEST);
  }

  @GetMapping(path = "/objectType/{objectTypeId}/rules")
  public CollectionModel<EntityModel<Rule>> allRules(@PathVariable Long objectTypeId) {
    ObjectType objectType = objectTypeRepository.findById(objectTypeId).orElseThrow(() ->
        new ObjectTypeNotFoundException(objectTypeId));
    List<EntityModel<Rule>> rules = ruleRepository.findByObjectTypeId(objectTypeId)
        .stream()
        .map(ruleModelAssembler::toModel)
        .collect(Collectors.toList());
    return CollectionModel.of(rules,
        linkTo(methodOn(RuleController.class).allRules(objectTypeId)).withSelfRel());
  }

  @PutMapping(path = "/objectType/{objectTypeId}/rule/{ruleId}")
  public ResponseEntity<String> updateRule(@PathVariable Long objectTypeId, @PathVariable Long ruleId,
      @RequestBody RuleSetupBody json) {
    if (!json.isValidForCreation()) {
      return createResponse("Given rule attributes are not valid for creation: " + json.toString(),
          HttpStatus.BAD_REQUEST);
    }
    ObjectType objectType = objectTypeRepository.findById(objectTypeId).orElseThrow(() ->
        new ObjectTypeNotFoundException(objectTypeId));
    ObjectType comparingObjectType = objectTypeRepository.findByNameIgnoreCase(json.getRuleName()).orElseThrow(() ->
        new ObjectTypeNotFoundException(json.getComparingObjectTypeName()));
    Rule rule = ruleRepository.findById(ruleId).orElseThrow(() -> new RuleNotFoundException(ruleId));
    if (!rule.getObjectTypeId().equals(objectType.getId())) {
      throw new RuleNotFoundException(objectTypeId, ruleId);
    }

    Rule newRule = createRule(json)
        .objectTypeId(objectType.getId())
        .comparingObjectTypeId(comparingObjectType.getId())
        .build();
    rule.merge(newRule);
    ruleRepository.save(rule);
    return createResponse("Rule updated: " + rule.toString(), HttpStatus.OK);
  }

  @GetMapping(path = "/objectType/{objectTypeId}/rule/{ruleId}")
  public EntityModel<Rule> oneRule(@PathVariable Long objectTypeId, @PathVariable Long ruleId) {
    ObjectType objectType = objectTypeRepository.findById(objectTypeId).orElseThrow(() ->
        new ObjectTypeNotFoundException(objectTypeId));
    Rule rule = ruleRepository.findById(ruleId).orElseThrow(() -> new RuleNotFoundException(ruleId));
    if (!rule.getObjectTypeId().equals(objectType.getId())) {
      throw new RuleNotFoundException(objectTypeId, ruleId);
    }
    return ruleModelAssembler.toModel(rule);
  }
}
