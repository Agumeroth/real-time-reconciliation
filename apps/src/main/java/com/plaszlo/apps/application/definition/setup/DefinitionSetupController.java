package com.plaszlo.apps.application.definition.setup;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author plaszlo
 */
@RestController
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class DefinitionSetupController {

  private final DefinitionSetupService service;

  @PostMapping(path = "/definitionSetup")
  public ResponseEntity<String> setupObjectTypes(@RequestBody DefinitionSetupBody json) {
    return service.processRequest(json);
  }

}
