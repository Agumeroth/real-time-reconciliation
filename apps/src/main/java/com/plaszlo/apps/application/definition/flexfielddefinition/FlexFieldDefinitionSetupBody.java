package com.plaszlo.apps.application.definition.flexfielddefinition;

import com.plaszlo.apps.domain.model.definition.flexfielddefinition.FlexFieldDefinition;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

/**
 * @author plaszlo
 */
public class FlexFieldDefinitionSetupBody extends FlexFieldDefinition {

  @Getter
  private String objectTypeName;

  public boolean isValidForCreation() {
    return StringUtils.isNotBlank(this.objectTypeName)
        && StringUtils.isNotBlank(this.getObjectAttributeName())
        && StringUtils.isNotBlank(this.getColumnName())
        && StringUtils.isNotBlank(this.getDerivationLogic());
  }
}
