package com.plaszlo.apps.application.definition.setup;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.plaszlo.apps.application.definition.flexfielddefinition.FlexFieldDefinitionSetupBody;
import com.plaszlo.apps.domain.model.definition.objecttype.ObjectType;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 * @author plaszlo
 */
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class DefinitionSetupBody {

  private List<ObjectType> objectTypes;

  private List<FlexFieldDefinitionSetupBody> flexFieldDefinitions;

}
