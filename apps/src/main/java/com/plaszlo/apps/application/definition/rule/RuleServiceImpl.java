package com.plaszlo.apps.application.definition.rule;

import com.plaszlo.apps.application.definition.objecttype.web.ObjectTypeNotFoundException;
import com.plaszlo.apps.domain.model.definition.objecttype.ObjectType;
import com.plaszlo.apps.domain.model.definition.objecttype.ObjectTypeRepository;
import com.plaszlo.apps.domain.model.definition.rule.Rule;
import com.plaszlo.apps.domain.model.definition.rule.Rule.RuleBuilder;
import com.plaszlo.apps.domain.model.definition.rule.RuleRepository;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author plaszlo
 */
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class RuleServiceImpl implements RuleService {

  private final ObjectTypeRepository objectTypeRepository;
  private final RuleRepository ruleRepository;

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public Optional<Rule> handleRuleCreation(ObjectType objectType, RuleSetupBody json) {
    ObjectType comparingObjectType = objectTypeRepository.findByNameIgnoreCase(json.getComparingObjectTypeName())
        .orElseThrow(() -> new ObjectTypeNotFoundException(json.getComparingObjectTypeName()));
    Rule rule = createRule(json)
        .objectTypeId(objectType.getId())
        .comparingObjectTypeId(comparingObjectType.getId())
        .build();

    Optional<Rule> existingRule = findExistingRule(rule);

    if (existingRule.isPresent()) {
      return Optional.empty();
    }
    return Optional.of(rule);
  }


  private Optional<Rule> findExistingRule(Rule rule) {
    return ruleRepository.findByRuleNameIgnoreCaseAndObjectTypeId(rule.getRuleName(), rule.getObjectTypeId());
  }

  public static RuleBuilder createRule(RuleSetupBody json) {
    return Rule.builder()
        .ruleName(json.getRuleName())
        .matchingRuleDefinition(json.getMatchingRuleDefinition())
        .ruleDefinition(json.getRuleDefinition())
        .oneToManyFlag(json.isOneToManyFlag());
  }
}
