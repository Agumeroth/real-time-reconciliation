package com.plaszlo.apps.application.execution.reconobject;

import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD1;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD10;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD11;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD12;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD13;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD14;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD15;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD2;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD3;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD4;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD5;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD6;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD7;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD8;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FLEX_FIELD9;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.NOT_FOR_EVALUATION;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.OBJECT_IDENTIFIER;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.PARENT_IDENTIFIER;
import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.VERSION;
import static com.plaszlo.apps.domain.model.execution.reconobject.StatusCode.RED;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.PathNotFoundException;
import com.jayway.jsonpath.spi.json.GsonJsonProvider;
import com.jayway.jsonpath.spi.json.JsonProvider;
import com.jayway.jsonpath.spi.mapper.GsonMappingProvider;
import com.jayway.jsonpath.spi.mapper.MappingProvider;
import com.plaszlo.apps.domain.model.definition.flexfielddefinition.FlexFieldDefinition;
import com.plaszlo.apps.domain.model.definition.flexfielddefinition.FlexFieldDefinitionRepository;
import com.plaszlo.apps.domain.model.definition.flexfielddefinition.FlexFieldType;
import com.plaszlo.apps.domain.model.definition.objecttype.ObjectType;
import com.plaszlo.apps.domain.model.definition.objecttype.ObjectTypeRepository;
import com.plaszlo.apps.domain.model.execution.reconobject.ReconObject;
import com.plaszlo.apps.domain.model.execution.reconobject.ReconObject.ReconObjectBuilder;
import com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectRepository;
import com.plaszlo.apps.domain.model.execution.reconobjectstage.ReconObjectStage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author plaszlo
 */
@Component
@Slf4j
public class ReconObjectDerivation {

  private final ObjectTypeRepository objectTypeRepository;
  private final FlexFieldDefinitionRepository flexFieldDefinitionRepository;
  private final ReconObjectRepository reconObjectRepository;
  private DocumentContext jsonPath;

  @Autowired
  public ReconObjectDerivation(final ObjectTypeRepository objectTypeRepository,
      final FlexFieldDefinitionRepository flexFieldDefinitionRepository,
      final ReconObjectRepository reconObjectRepository) {
    this.objectTypeRepository = objectTypeRepository;
    this.flexFieldDefinitionRepository = flexFieldDefinitionRepository;
    this.reconObjectRepository = reconObjectRepository;
    setJsonPathConfig();
  }

  /**
   * @param stage
   * @return
   */
  @Transactional(propagation = Propagation.REQUIRES_NEW, noRollbackFor = Exception.class)
  public List<ReconObject> derive(ReconObjectStage stage) {
    log.info("Deriving new ReconObject for ReconObjectStage: {}", stage.getId());
    List<ReconObject> createdObjects = new ArrayList<>();

    objectTypeRepository.findAllByEnabledFlag(true).forEach(objectType ->
        createdObjects.addAll(tryObjectTypeForStage(stage, objectType)));

    return createdObjects;
  }

  /**
   * @param stage
   * @param objectType
   */
  private List<ReconObject> tryObjectTypeForStage(ReconObjectStage stage, ObjectType objectType) {
    List<ReconObject> resultList = new ArrayList<>();

    String messageString = stage.getMessage();
    jsonPath = JsonPath.parse(messageString);
    JsonArray jsonMessage;
    try {
      jsonMessage = jsonPath.read(objectType.getDerivationLogic());
      if (jsonMessage.get(0) instanceof JsonNull) {
        log.info("Incoming message is not {} ObjectType", objectType.getName());
        return resultList;
      }
    } catch (PathNotFoundException e) {
      log.info("Incoming message is not {} ObjectType", objectType.getName());
      return resultList;
    }

    List<String> excludedFields = Collections.emptyList();
    if (StringUtils.isNotBlank(objectType.getExcludedFields())) {
      excludedFields = Arrays.asList(objectType.getExcludedFields().split(","));
    }

    int numberOfNeededObjects = 1;
    if (jsonMessage.get(0) instanceof JsonArray) {
      numberOfNeededObjects = jsonMessage.get(0).getAsJsonArray().size();
    }

    for (int i = 0; i < numberOfNeededObjects; i++) {
      try {
        createOrUpdateObject(stage, objectType, excludedFields, jsonMessage, i).ifPresent(resultList::add);
      } catch (Exception e) {
        log.error("Exception during ReconObject creation: {}", e.toString());
      }
    }

    return resultList;
  }

  //TODO what to do when exception occurred during object creation?

  /**
   * Current logic: Create all objects if possible, leave the rest
   *
   * @param stage
   * @param objectType
   * @param excludedFields
   * @param jsonMessage
   * @param i
   * @return
   */
  private Optional<ReconObject> createOrUpdateObject(ReconObjectStage stage, ObjectType objectType,
      List<String> excludedFields, JsonArray jsonMessage, int i) {
    List<FlexFieldDefinition> flexFieldDefs = flexFieldDefinitionRepository
        .findByObjectTypeId(objectType.getId());
    String messageOfObject = this.extractMessage(excludedFields, jsonMessage, i);
    ReconObject newReconObject = this.createObject(stage, flexFieldDefs, objectType, i, messageOfObject).build();
    Optional<ReconObject> existing = this.findExistingReconObject(newReconObject);

    if (existing.isPresent()) {
      ReconObject existingObject = existing.get();
      boolean mergeSuccess = existingObject.merge(newReconObject);
      log.info("Merge initiated for ReconObject with id {}. Success: {}", existingObject.getId(), mergeSuccess);
      if (!mergeSuccess) {
        return Optional.empty();
      }
      return Optional.of(existingObject);
    } else {
      reconObjectRepository.save(newReconObject);
      log.info("ReconObject created with id {} and objectIdentifier {}",
          newReconObject.getId(), newReconObject.getFlexField(OBJECT_IDENTIFIER, NOT_FOR_EVALUATION));
      return Optional.of(newReconObject);
    }
  }

  /**
   * @param excludedFields
   * @param jsonMessage
   * @param number
   * @return
   */
  private String extractMessage(List<String> excludedFields, JsonArray jsonMessage, int number) {
    JsonObject jsonObject = getJsonObjectFromMessage(jsonMessage, number);
    if (excludedFields.isEmpty()) {
      return jsonObject.toString();
    }
    excludedFields.forEach(e -> {
      if (jsonObject.keySet().contains(e)) {
        log.info("Removing {} excluded attribute from the message", e);
        jsonObject.remove(e);
      }
    });
    return jsonObject.toString();
  }

  /**
   * @param reconObject
   * @return
   */
  private Optional<ReconObject> findExistingReconObject(ReconObject reconObject) {
    return reconObjectRepository.findByObjectTypeIdAndObjectIdentifier(
        reconObject.getObjectTypeId(), reconObject.getFlexField(OBJECT_IDENTIFIER, NOT_FOR_EVALUATION));
  }

  /**
   * @param jsonMessage
   * @param number
   * @return
   */
  private JsonObject getJsonObjectFromMessage(JsonArray jsonMessage, int number) {
    if (jsonMessage.get(0) instanceof JsonArray) {
      return jsonMessage.get(0).getAsJsonArray().get(number).getAsJsonObject();
    }
    return jsonMessage.get(0).getAsJsonObject();
  }

  /**
   * @param derivationLogic
   * @param number
   * @return
   */
  private JsonElement readJsonElement(String derivationLogic, int number) {
    JsonArray jsonArray = jsonPath.read(actualizeDerivationLogic(derivationLogic, number));
    return jsonArray.get(0);
  }

  /**
   * @param list
   * @param flexField
   * @param number
   * @return
   */
  private String getFlexField(List<FlexFieldDefinition> list, String flexField, int number) {
    Optional<FlexFieldDefinition> result = filterList(list, flexField);
    return result.map(flexFieldDefinition ->
        readJsonElement(flexFieldDefinition.getDerivationLogic(), number).getAsString())
        .orElse(null);
  }

  /**
   * @param list
   * @param flexField
   * @return
   */
  private FlexFieldType getFlexFieldType(List<FlexFieldDefinition> list, String flexField) {
    Optional<FlexFieldDefinition> result = filterList(list, flexField);
    return result.map(FlexFieldDefinition::getColumnType).orElse(null);
  }

  /**
   * @param list
   * @param flexField
   * @return
   */
  private Optional<FlexFieldDefinition> filterList(List<FlexFieldDefinition> list, String flexField) {
    return list.stream()
        .filter(f -> flexField.equalsIgnoreCase(f.getColumnName()))
        .findFirst();
  }

  /**
   * @param derivationLogic
   * @param number
   * @return
   */
  private String actualizeDerivationLogic(String derivationLogic, int number) {
    return derivationLogic.replace("?", Integer.toString(number));
  }

  /**
   * @param flexFieldDefinitions
   * @param objectType
   * @param number
   * @param message
   * @return
   */
  private ReconObjectBuilder createObject(ReconObjectStage stage, List<FlexFieldDefinition> flexFieldDefinitions,
      ObjectType objectType, int number, String message) {
    return ReconObject.builder()
        .objectTypeId(objectType.getId())
        .objectStageId(stage.getId())
        .message(message)
        .objectIdentifier(getFlexField(flexFieldDefinitions, OBJECT_IDENTIFIER, number))
        .parentIdentifier(getFlexField(flexFieldDefinitions, PARENT_IDENTIFIER, number))
        .objectVersion(getFlexField(flexFieldDefinitions, VERSION, number))
        .objectIdentifierType(getFlexFieldType(flexFieldDefinitions, OBJECT_IDENTIFIER))
        .parentIdentifierType(getFlexFieldType(flexFieldDefinitions, PARENT_IDENTIFIER))
        .versionType(getFlexFieldType(flexFieldDefinitions, VERSION))
        .statusCode(RED)
        .flexField1(getFlexField(flexFieldDefinitions, FLEX_FIELD1, number))
        .flexFieldType1(getFlexFieldType(flexFieldDefinitions, FLEX_FIELD1))
        .flexField2(getFlexField(flexFieldDefinitions, FLEX_FIELD2, number))
        .flexFieldType2(getFlexFieldType(flexFieldDefinitions, FLEX_FIELD2))
        .flexField3(getFlexField(flexFieldDefinitions, FLEX_FIELD3, number))
        .flexFieldType3(getFlexFieldType(flexFieldDefinitions, FLEX_FIELD3))
        .flexField4(getFlexField(flexFieldDefinitions, FLEX_FIELD4, number))
        .flexFieldType4(getFlexFieldType(flexFieldDefinitions, FLEX_FIELD4))
        .flexField5(getFlexField(flexFieldDefinitions, FLEX_FIELD5, number))
        .flexFieldType5(getFlexFieldType(flexFieldDefinitions, FLEX_FIELD5))
        .flexField6(getFlexField(flexFieldDefinitions, FLEX_FIELD6, number))
        .flexFieldType6(getFlexFieldType(flexFieldDefinitions, FLEX_FIELD6))
        .flexField7(getFlexField(flexFieldDefinitions, FLEX_FIELD7, number))
        .flexFieldType7(getFlexFieldType(flexFieldDefinitions, FLEX_FIELD7))
        .flexField8(getFlexField(flexFieldDefinitions, FLEX_FIELD8, number))
        .flexFieldType8(getFlexFieldType(flexFieldDefinitions, FLEX_FIELD8))
        .flexField9(getFlexField(flexFieldDefinitions, FLEX_FIELD9, number))
        .flexFieldType9(getFlexFieldType(flexFieldDefinitions, FLEX_FIELD9))
        .flexField10(getFlexField(flexFieldDefinitions, FLEX_FIELD10, number))
        .flexFieldType10(getFlexFieldType(flexFieldDefinitions, FLEX_FIELD10))
        .flexField11(getFlexField(flexFieldDefinitions, FLEX_FIELD11, number))
        .flexFieldType11(getFlexFieldType(flexFieldDefinitions, FLEX_FIELD11))
        .flexField12(getFlexField(flexFieldDefinitions, FLEX_FIELD12, number))
        .flexFieldType12(getFlexFieldType(flexFieldDefinitions, FLEX_FIELD12))
        .flexField13(getFlexField(flexFieldDefinitions, FLEX_FIELD13, number))
        .flexFieldType13(getFlexFieldType(flexFieldDefinitions, FLEX_FIELD13))
        .flexField14(getFlexField(flexFieldDefinitions, FLEX_FIELD14, number))
        .flexFieldType14(getFlexFieldType(flexFieldDefinitions, FLEX_FIELD14))
        .flexField15(getFlexField(flexFieldDefinitions, FLEX_FIELD15, number))
        .flexFieldType15(getFlexFieldType(flexFieldDefinitions, FLEX_FIELD15));
  }

  /**
   *
   */
  private void setJsonPathConfig() {
    Configuration.setDefaults(new Configuration.Defaults() {

      private final JsonProvider jsonProvider = new GsonJsonProvider(new GsonBuilder().create());
      private final MappingProvider mappingProvider = new GsonMappingProvider();
      private final Set<Option> options = new LinkedHashSet<>();

      @Override
      public JsonProvider jsonProvider() {
        return jsonProvider;
      }

      @Override
      public Set<Option> options() {
        options.add(Option.ALWAYS_RETURN_LIST);
        options.add(Option.DEFAULT_PATH_LEAF_TO_NULL);
        return options;
      }

      @Override
      public MappingProvider mappingProvider() {
        return mappingProvider;
      }
    });
  }


}
