package com.plaszlo.apps.application.definition.flexfielddefinition.web;

public class FlexFieldDefinitionNotFoundException extends RuntimeException {

  public FlexFieldDefinitionNotFoundException(Long id) {
    super("Could not find FlexFieldDefinition " + id);
  }

  public FlexFieldDefinitionNotFoundException(Long objectTypeId, Long ffId) {
    super("Could not find FlexFieldDefinition " + ffId + " for ObjectType " + objectTypeId);
  }
}
