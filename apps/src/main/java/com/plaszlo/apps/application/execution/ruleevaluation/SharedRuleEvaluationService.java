package com.plaszlo.apps.application.execution.ruleevaluation;

import static com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectConstants.FOR_EVALUATION;

import com.plaszlo.apps.domain.model.definition.flexfielddefinition.FlexFieldDefinition;
import com.plaszlo.apps.domain.model.definition.flexfielddefinition.FlexFieldDefinitionRepository;
import com.plaszlo.apps.domain.model.definition.objecttype.ObjectType;
import com.plaszlo.apps.domain.model.definition.objecttype.ObjectTypeRepository;
import com.plaszlo.apps.domain.model.execution.reconobject.ReconObject;
import com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectRepository;
import com.plaszlo.apps.domain.model.execution.ruleexecution.RuleExecution;
import com.plaszlo.apps.domain.model.execution.ruleexecution.RuleExecutionRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author plaszlo
 */
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public abstract class SharedRuleEvaluationService {

  private final ReconObjectRepository objectRepository;
  private final ObjectTypeRepository objectTypeRepository;
  private final FlexFieldDefinitionRepository flexFieldDefinitionRepository;
  private final RuleExecutionRepository executionRepository;

  Map<String, Object> createMap(ReconObject object) {
    Map<String, Object> result = new HashMap<>();
    Optional<ObjectType> otOptional = objectTypeRepository.findById(object.getObjectTypeId());

    if (otOptional.isEmpty()) {
      return result;
    }

    ObjectType ot = otOptional.get();
    List<FlexFieldDefinition> flexFields = flexFieldDefinitionRepository
        .findByObjectTypeId(object.getObjectTypeId());
    if (flexFields.isEmpty()) {
      return result;
    }

    flexFields.forEach(d -> {
      String key = ot.getName().toUpperCase().concat(d.getObjectAttributeName().toUpperCase());
      String value = object.getFlexField(d.getColumnName(), FOR_EVALUATION);
      result.put(key, value);
    });
    return result;
  }

  List<ReconObject> findComparableObjects(Long comparableObjectTypeId) {
    return objectRepository.findByObjectTypeId(comparableObjectTypeId);
  }

  void updateObjectStatus(ReconObject object) {
    log.info("Checking RuleExecutions for ReconObject {}", object.getId());
    List<RuleExecution> ruleExecutions = findExecutionsForObject(object.getId());
    object.updateStatus(ruleExecutions);
  }

  List<RuleExecution> findExecutionsForObject(Long objectId) {
    return executionRepository.findByObjectId(objectId);
  }

  String refineDefinition(String value) {
    return value.replace(":", "").toUpperCase();
  }

  Map<String, Object> mergeMap(Map<String, Object> map1, Map<String, Object> map2) {
    Map<String, Object> result = map1;
    result.putAll(map2);
    return result;
  }

  String createEvaluatedRule(String ruleToEvaluate, Map<String, Object> map) {
    return StringUtils.replaceEach(ruleToEvaluate, map.keySet().toArray(new String[0]),
        map.values().stream().map(Object::toString).toArray(String[]::new));
  }

  @AllArgsConstructor
  @Getter
  public static class EvaluationResult {

    private final List<Long> comparingObjectIds;
    private final Boolean result;
    private final String evaluatedRule;
  }
}
