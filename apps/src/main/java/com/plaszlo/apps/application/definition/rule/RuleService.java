package com.plaszlo.apps.application.definition.rule;

import com.plaszlo.apps.domain.model.definition.objecttype.ObjectType;
import com.plaszlo.apps.domain.model.definition.rule.Rule;
import java.util.Optional;

/**
 * @author plaszlo
 */
public interface RuleService {

  Optional<Rule> handleRuleCreation(ObjectType objectType, RuleSetupBody json);

}
