package com.plaszlo.apps.application.definition.objecttype.web;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.plaszlo.apps.domain.model.definition.objecttype.ObjectType;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

@Component
public class ObjectTypeModelAssembler implements RepresentationModelAssembler<ObjectType, EntityModel<ObjectType>> {

  @Override
  public EntityModel<ObjectType> toModel(ObjectType entity) {
    return EntityModel.of(entity,
        WebMvcLinkBuilder.linkTo(methodOn(ObjectTypeController.class).oneObjectType(entity.getId())).withSelfRel(),
        linkTo(methodOn(ObjectTypeController.class).allObjectTypes()).withRel("objectTypes"));
  }
}
