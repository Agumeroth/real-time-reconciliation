package com.plaszlo.apps.application.shared;

import lombok.extern.slf4j.Slf4j;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.listener.RetryListenerSupport;

/**
 * @author plaszlo
 */
@Slf4j
public class DefaultListenerSupport extends RetryListenerSupport {

  @Override
  public <T, E extends Throwable> void close(RetryContext context, RetryCallback<T, E> callback, Throwable throwable) {

    log.debug("onClose -- retry count: {}", context != null ? context.getRetryCount() : "null");
    log.debug("onClose -- message: {}", throwable != null ? throwable.getMessage() : "null");
    log.debug("onClose -- class: {}", throwable != null ? throwable.getClass() : "null");
    log.debug("onClose -- stack trace: {}", throwable != null ? throwable.getStackTrace() : "null");

    super.close(context, callback, throwable);
  }

  @Override
  public <T, E extends Throwable> void onError(RetryContext context, RetryCallback<T, E> callback,
      Throwable throwable) {

    log.debug("onError -- retry count: {}", context != null ? context.getRetryCount() : "null");
    log.debug("onError -- message: {}", throwable != null ? throwable.getMessage() : "null");
    log.debug("onError -- class: {}", throwable != null ? throwable.getClass() : "null");
    log.debug("onError -- stack trace: {}", throwable != null ? throwable.getStackTrace() : "null");

    super.onError(context, callback, throwable);
  }

  @Override
  public <T, E extends Throwable> boolean open(RetryContext context, RetryCallback<T, E> callback) {

    log.debug("onOpen -- Opening retry block");

    return super.open(context, callback);
  }
}
