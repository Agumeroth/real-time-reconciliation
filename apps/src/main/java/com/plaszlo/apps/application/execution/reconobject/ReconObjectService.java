package com.plaszlo.apps.application.execution.reconobject;

import com.plaszlo.apps.application.execution.reconobjectstage.ReconObjectStagePersistedEvent;

/**
 * @author plaszlo
 */
public interface ReconObjectService {

  void handleObjectStagePersistedEvent(ReconObjectStagePersistedEvent event);
}
