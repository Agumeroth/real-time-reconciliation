package com.plaszlo.apps.application.execution.ruleevaluation;

import static com.plaszlo.apps.application.execution.ruleevaluation.RuleEvaluationServiceImpl.createNewExecution;

import com.plaszlo.apps.domain.model.definition.flexfielddefinition.FlexFieldDefinitionRepository;
import com.plaszlo.apps.domain.model.definition.objecttype.ObjectTypeRepository;
import com.plaszlo.apps.domain.model.definition.rule.Rule;
import com.plaszlo.apps.domain.model.execution.reconobject.ReconObject;
import com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectRepository;
import com.plaszlo.apps.domain.model.execution.ruleexecution.RuleExecution;
import com.plaszlo.apps.domain.model.execution.ruleexecution.RuleExecutionRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author plaszlo
 */
@Service
@Slf4j
public class OneToManyService extends SharedRuleEvaluationService {

  private final RuleExecutionRepository ruleExecutionRepository;

  public OneToManyService(
      ReconObjectRepository reconObjectRepository,
      ObjectTypeRepository objectTypeRepository,
      FlexFieldDefinitionRepository flexFieldDefinitionRepository,
      RuleExecutionRepository ruleExecutionRepository) {
    super(reconObjectRepository, objectTypeRepository, flexFieldDefinitionRepository, ruleExecutionRepository);
    this.ruleExecutionRepository = ruleExecutionRepository;
  }

  public Optional<RuleExecution> createOrUpdateRuleExecution(ReconObject object, Rule rule) {
    log.info("One to Many type Rule {} found for Object Id {}", rule.getRuleName(), object.getId());
    Optional<RuleExecution> execution = processRuleForObject(rule, object);
    if (execution.isPresent()) {
      ruleExecutionRepository.save(execution.get());
      this.updateObjectStatus(object);
    }
    return execution;
  }

  private Optional<RuleExecution> processRuleForObject(Rule rule, ReconObject object) {
    EvaluationResult evaluationResult = evaluateRuleForObject(rule, object);
    if (evaluationResult == null) {
      return Optional.empty();
    }

    Optional<RuleExecution> existingExecutionOp = findExistingRuleExecution(rule.getId(), object.getId());

    if (existingExecutionOp.isPresent()) {
      RuleExecution existingExecution = existingExecutionOp.get();
      existingExecution.updateAttributes(evaluationResult);
      return Optional.of(existingExecution);
    }

    log.info("Creating RuleExecution for Object id {} and Rule {}", object.getId(), rule.getRuleName());
    RuleExecution newExecution = createNewExecution(object, rule, evaluationResult).build();
    return Optional.of(newExecution);
  }

  private EvaluationResult evaluateRuleForObject(Rule rule, ReconObject object) {
    List<ReconObject> comparableObjects = this.findComparableObjects(rule.getComparingObjectTypeId());
    List<String> resultList = new ArrayList<>();
    List<Long> comparingObjectIds = new ArrayList<>();

    Map<String, Object> objectValueMap = this.createMap(object);
    if (objectValueMap.isEmpty() || comparableObjects.isEmpty()) {
      log.debug("Value map or comparable Objects could not be found for Object Id {} with Object Type Id {}",
          object.getId(), object.getObjectTypeId());
      return null;
    }

    String ruleDefinition = this.refineDefinition(rule.getRuleDefinition());
    String matchingRule = this.refineDefinition(rule.getMatchingRuleDefinition());

    comparableObjects.forEach(o -> {
      Map<String, Object> comparingObjectMap = this.createMap(o);
      if (comparingObjectMap.isEmpty()) {
        log.debug("Comparing Object value map not found for Object Id {} with Object Type Id {}",
            o.getId(), o.getObjectTypeId());
        return;
      }
      Map<String, Object> completeMap = this.mergeMap(objectValueMap, comparingObjectMap);
      boolean isMatching = rule.evaluateRule(matchingRule, completeMap);

      if (isMatching) {
        log.debug("Found matching Object with Id {} for Object Id {}", object.getId(), o.getId());
        comparingObjectMap.keySet()
            .forEach(k -> {
              if (ruleDefinition.contains(k)) {
                resultList.add(comparingObjectMap.get(k).toString());
              }
            });
        comparingObjectIds.add(o.getId());
      }
    });
    if (resultList.isEmpty()) {
      log.debug("No matching Objects found for Object Id {}", object.getId());
      return null;
    }
    String ruleToEvaluate = makeRuleForEvaluation(ruleDefinition);
    objectValueMap.put("$$", resultList);
    Boolean evaluationResult = rule.evaluateRule(ruleToEvaluate, objectValueMap);
    String evaluatedRule = createEvaluatedRule(ruleToEvaluate, objectValueMap);
    return new EvaluationResult(comparingObjectIds, evaluationResult, evaluatedRule);
  }

  private Optional<RuleExecution> findExistingRuleExecution(Long ruleId, Long objectId) {
    return ruleExecutionRepository.findByRuleIdAndObjectId(ruleId, objectId).stream().findFirst();
  }

  private String makeRuleForEvaluation(String definition) {
    int firstIndex = definition.indexOf("[") + 1;
    int lastIndex = definition.indexOf("]");
    return definition.substring(0, firstIndex).concat(definition.substring(lastIndex))
        .replace("[]", "$$");
  }

}
