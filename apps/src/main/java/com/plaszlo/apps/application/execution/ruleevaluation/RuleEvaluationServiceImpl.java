package com.plaszlo.apps.application.execution.ruleevaluation;

import static com.plaszlo.apps.domain.model.execution.reconobject.StatusCode.RED;

import com.plaszlo.apps.application.execution.ruleevaluation.SharedRuleEvaluationService.EvaluationResult;
import com.plaszlo.apps.domain.model.definition.rule.Rule;
import com.plaszlo.apps.domain.model.definition.rule.RuleRepository;
import com.plaszlo.apps.domain.model.execution.reconobject.ReconObject;
import com.plaszlo.apps.domain.model.execution.reconobject.ReconObjectRepository;
import com.plaszlo.apps.domain.model.execution.reconobject.StatusCode;
import com.plaszlo.apps.domain.model.execution.ruleexecution.RuleExecution;
import com.plaszlo.apps.domain.model.execution.ruleexecution.RuleExecution.RuleExecutionBuilder;
import com.plaszlo.apps.domain.model.execution.ruleexecution.RuleExecutionRepository;
import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author plaszlo
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class RuleEvaluationServiceImpl {

  private final RuleRepository ruleRepository;
  private final OneToManyService oneToManyService;
  private final OneToOneService oneToOneService;
  private final RuleExecutionRepository executionRepository;
  private final ReconObjectRepository objectRepository;
  private final RetryTemplate retryTemplate;

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void evaluateRulesForObject(ReconObject object) {
    log.info("Handling Rule Execution creation for Object with id {} started", object.getId());
    retryTemplate.execute(c -> {
      doEvaluation(object);
      return null;
    });
  }

  private void doEvaluation(ReconObject object) {
    List<Rule> rules = findRulesForObject(object.getObjectTypeId());
    if (rules.isEmpty()) {
      object.statusNoRules();
      objectRepository.save(object);
    }
    rules.forEach(r -> {
      List<RuleExecution> createdExecutions = new ArrayList<>();
      RuleExecution placeholderExecution = createPlaceholderExecution(object, r);
      log.info("Placeholder RuleExecution created with objectId {} and ruleId {}", object.getId(), r.getId());
      if (r.isOneToManyFlag()) {
        oneToManyService.createOrUpdateRuleExecution(object, r).ifPresent(createdExecutions::add);
      } else {
        List<RuleExecution> executions = oneToOneService.createOrUpdateRuleExecutions(object, r);
        if (!executions.isEmpty()) {
          createdExecutions.addAll(executions);
        }
      }

      if (createdExecutions.isEmpty()) {
        log.info("No RuleExecutions created for Rule {}, saving placeholder execution.", r.getRuleName());
        executionRepository.save(placeholderExecution);
      } else {
        log.info("{} RuleExecution(s) created for Rule {}", createdExecutions.size(), r.getRuleName());
      }
    });
    handleObjectAsComparing(object);
  }

  private void handleObjectAsComparing(ReconObject object) {
    List<Rule> rules = findRuleForComparingObject(object.getObjectTypeId());
    rules.forEach(r -> {
      List<RuleExecution> executions = new ArrayList<>();
      List<ReconObject> existingObjectsForRule = findExistingObjectsForRule(r.getObjectTypeId());
      existingObjectsForRule.forEach(o -> {
        if (r.isOneToManyFlag()) {
          oneToManyService.createOrUpdateRuleExecution(o, r).ifPresent(executions::add);
        } else {
          executions.addAll(oneToOneService.createOrUpdateRuleExecutions(o, r));
        }
      });
      if (!executions.isEmpty()) {
        executionRepository.saveAll(executions);
      }
    });
  }

  private List<Rule> findRuleForComparingObject(Long comparingObjectTypeId) {
    return ruleRepository.findByComparingObjectTypeId(comparingObjectTypeId);
  }

  private List<ReconObject> findExistingObjectsForRule(Long objectTypeId) {
    return objectRepository.findByObjectTypeId(objectTypeId);
  }

  /**
   * Find Rules for the given objectTypeId
   *
   * @param objectTypeId ObjectTypeId
   * @return List of Rules
   */
  private List<Rule> findRulesForObject(Long objectTypeId) {
    return ruleRepository.findByObjectTypeId(objectTypeId);
  }

  private RuleExecution createPlaceholderExecution(ReconObject object, Rule rule) {
    return RuleExecution.builder()
        .objectId(object.getId())
        .ruleId(rule.getId())
        .evaluatedRule(rule.getRuleDefinition())
        .statusCode(RED)
        .statusMessage("Placeholder RuleExecution")
        .build();
  }

  static RuleExecutionBuilder createNewExecution(ReconObject object, Rule rule, EvaluationResult evaluationResult) {
    return RuleExecution.builder()
        .objectId(object.getId())
        .ruleId(rule.getId())
        .comparingObjectId(evaluationResult.getComparingObjectIds())
        .evaluatedRule(evaluationResult.getEvaluatedRule())
        .statusCode(StatusCode.from(evaluationResult.getResult()))
        .statusMessage(null);
  }

}
