package com.plaszlo.apps.application.definition.rule.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class RuleNotFoundAdvice {

  @ResponseBody
  @ExceptionHandler(RuleNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  String ruleNotFoundHandler(RuleNotFoundException e) {
    return e.getMessage();
  }
}
