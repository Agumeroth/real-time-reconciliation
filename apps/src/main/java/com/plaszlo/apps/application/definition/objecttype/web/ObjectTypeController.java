package com.plaszlo.apps.application.definition.objecttype.web;

import static com.plaszlo.apps.application.execution.reconobjectstage.reprocess.ReconObjectStageReprocessService.createResponse;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.plaszlo.apps.application.definition.objecttype.ObjectTypeService;
import com.plaszlo.apps.application.definition.setup.DefinitionSetupService;
import com.plaszlo.apps.application.definition.setup.web.DefinitionSetupBody;
import com.plaszlo.apps.domain.model.definition.objecttype.ObjectType;
import com.plaszlo.apps.domain.model.definition.objecttype.ObjectTypeRepository;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class ObjectTypeController {

  private final DefinitionSetupService setupService;
  private final ObjectTypeService service;
  private final ObjectTypeModelAssembler objectTypeModelAssembler;
  private final ObjectTypeRepository objectTypeRepository;

  @PostMapping(path = "/objectType/setupAll")
  public ResponseEntity<String> setupObjectTypeWithAllElements(@RequestBody DefinitionSetupBody json) {
    return setupService.processRequest(json);
  }

  @PostMapping(path = "/objectType")
  public ResponseEntity<String> setupObjectType(@RequestBody ObjectType json) {
    if (!json.isValidForCreation()) {
      return createResponse("Given objectType attributes are not valid for creation: " + json.toString(),
          HttpStatus.BAD_REQUEST);
    }
    Optional<ObjectType> createdObjectType = service.handleObjectTypeCreation(json);

    if (createdObjectType.isPresent()) {
      return createResponse("ObjectType created: " + json.toString(), HttpStatus.OK);
    }
    return createResponse("ObjectType " + json.getName() + " already exists!", HttpStatus.BAD_REQUEST);
  }

  @PutMapping(path = "/objectType/{id}")
  public ResponseEntity<String> updateObjectType(@RequestBody ObjectType json, @PathVariable Long id) {
    ObjectType objectType = objectTypeRepository.findById(id).orElseThrow(() -> new ObjectTypeNotFoundException(id));

    if (json.isValidForCreation()) {
      objectType.merge(json);
      objectTypeRepository.save(objectType);
      return createResponse("ObjectType updated: " + objectType.toString(), HttpStatus.OK);
    }
    return createResponse("Given objectType attributes are not valid for creation: " + json.toString(),
        HttpStatus.BAD_REQUEST);
  }


  @GetMapping(path = "/objectTypes")
  public CollectionModel<EntityModel<ObjectType>> allObjectTypes() {
    List<EntityModel<ObjectType>> objectTypes = objectTypeRepository.findAll()
        .stream()
        .map(objectTypeModelAssembler::toModel)
        .collect(Collectors.toList());
    return CollectionModel.of(objectTypes,
        linkTo(methodOn(ObjectTypeController.class).allObjectTypes()).withSelfRel());
  }

  @GetMapping(path = "/objectType/{id}")
  public EntityModel<ObjectType> oneObjectType(@PathVariable Long id) {
    ObjectType objectType = objectTypeRepository.findById(id).orElseThrow(() -> new ObjectTypeNotFoundException(id));
    return objectTypeModelAssembler.toModel(objectType);
  }
}
