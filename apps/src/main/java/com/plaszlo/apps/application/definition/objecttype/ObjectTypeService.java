package com.plaszlo.apps.application.definition.objecttype;

import com.plaszlo.apps.domain.model.definition.objecttype.ObjectType;
import java.util.Optional;

/**
 * @author plaszlo
 */
public interface ObjectTypeService {

  Optional<ObjectType> handleObjectTypeCreation(ObjectType objectType);
}
