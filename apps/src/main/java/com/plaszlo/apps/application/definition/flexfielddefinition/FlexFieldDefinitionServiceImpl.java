package com.plaszlo.apps.application.definition.flexfielddefinition;

import static com.plaszlo.apps.domain.model.definition.flexfielddefinition.FlexFieldType.STRING;

import com.plaszlo.apps.domain.model.definition.flexfielddefinition.FlexFieldDefinition;
import com.plaszlo.apps.domain.model.definition.flexfielddefinition.FlexFieldDefinition.FlexFieldDefinitionBuilder;
import com.plaszlo.apps.domain.model.definition.flexfielddefinition.FlexFieldDefinitionRepository;
import com.plaszlo.apps.domain.model.definition.objecttype.ObjectType;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author plaszlo
 */
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class FlexFieldDefinitionServiceImpl implements FlexFieldDefinitionService {

  private final FlexFieldDefinitionRepository repository;

  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public Optional<FlexFieldDefinition> handleFlexFieldDefinitionCreation(ObjectType objectType,
      FlexFieldDefinition flexFieldDefinition) {
    Long objectTypeId = objectType.getId();
    FlexFieldDefinition newFlexFieldDefinition = this.createFlexFieldDefinition(flexFieldDefinition, objectTypeId)
        .build();

    Optional<FlexFieldDefinition> existingDefinition = findExistingFlexFieldDefinition(newFlexFieldDefinition);
    if (existingDefinition.isPresent()) {
      log.info("FlexFieldDefinition with objectTypeId: {} and column name: {} already exists, merge not allowed.",
          existingDefinition.get().getObjectTypeId(), existingDefinition.get().getColumnName());
      return Optional.empty();
    }

    return Optional.of(newFlexFieldDefinition);
  }

  private Optional<FlexFieldDefinition> findExistingFlexFieldDefinition(FlexFieldDefinition newFlexFieldDefinition) {
    return repository.findByObjectTypeIdAndColumnName(newFlexFieldDefinition.getObjectTypeId(),
        newFlexFieldDefinition.getColumnName());
  }

  public FlexFieldDefinitionBuilder createFlexFieldDefinition(FlexFieldDefinition flexFieldDefinition,
      Long objectTypeId) {
    return FlexFieldDefinition.builder()
        .objectTypeId(objectTypeId)
        .columnName(flexFieldDefinition.getColumnName())
        .derivationLogic(flexFieldDefinition.getDerivationLogic())
        .columnType(ObjectUtils.defaultIfNull(flexFieldDefinition.getColumnType(), STRING))
        .objectAttributeName(flexFieldDefinition.getObjectAttributeName());
  }
}
