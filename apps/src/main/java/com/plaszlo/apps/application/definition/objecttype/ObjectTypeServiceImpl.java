package com.plaszlo.apps.application.definition.objecttype;

import com.plaszlo.apps.domain.model.definition.objecttype.ObjectType;
import com.plaszlo.apps.domain.model.definition.objecttype.ObjectTypeRepository;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author plaszlo
 */
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class ObjectTypeServiceImpl implements ObjectTypeService {

  private final ObjectTypeRepository repository;

  /**
   * @param newObjectType
   * @return
   */
  @Override
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public Optional<ObjectType> handleObjectTypeCreation(ObjectType newObjectType) {
    Optional<ObjectType> existing = findExistingObjectType(newObjectType);

    if (existing.isPresent()) {
      log.info("ObjectType {} already exists, ObjectType creation failed.", existing.get().getName());
      return Optional.empty();
    }

    return Optional.of(repository.save(newObjectType));
  }

  /**
   * @param objectType
   * @return
   */
  Optional<ObjectType> findExistingObjectType(ObjectType objectType) {
    return repository.findByNameIgnoreCase(objectType.getName());
  }
}
