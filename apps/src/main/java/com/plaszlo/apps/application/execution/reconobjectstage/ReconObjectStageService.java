package com.plaszlo.apps.application.execution.reconobjectstage;

import org.apache.kafka.clients.consumer.ConsumerRecord;

/**
 * @author plaszlo
 */
public interface ReconObjectStageService {

  void handle(ConsumerRecord<String, ?> record);
}
