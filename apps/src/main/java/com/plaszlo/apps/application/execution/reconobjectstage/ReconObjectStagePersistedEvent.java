package com.plaszlo.apps.application.execution.reconobjectstage;

import com.plaszlo.apps.domain.model.execution.reconobjectstage.ReconObjectStage;
import com.plaszlo.apps.domain.shared.DomainEvent;
import java.time.Instant;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * @author plaszlo
 */
@Getter
@ToString
@RequiredArgsConstructor(staticName = "of")
public class ReconObjectStagePersistedEvent implements DomainEvent {

  private final Instant occurredOn = Instant.now();
  private final long objectStageId;
  private final ReconObjectStage reconObjectStage;

}
