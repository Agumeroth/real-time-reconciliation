package com.plaszlo.apps.application.definition.setup;

import static com.plaszlo.apps.application.execution.reconobjectstage.reprocess.ReconObjectStageReprocessService.createResponse;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.OK;

import com.plaszlo.apps.application.definition.objecttype.ObjectTypeService;
import com.plaszlo.apps.application.definition.rule.RuleSetupBody;
import com.plaszlo.apps.application.definition.setup.web.DefinitionSetupBody;
import com.plaszlo.apps.application.execution.reconobjectstage.reprocess.ReconObjectStageReprocessService;
import com.plaszlo.apps.domain.model.definition.flexfielddefinition.FlexFieldDefinition;
import com.plaszlo.apps.domain.model.definition.flexfielddefinition.FlexFieldDefinitionRepository;
import com.plaszlo.apps.domain.model.definition.objecttype.ObjectType;
import com.plaszlo.apps.domain.model.definition.objecttype.ObjectTypeRepository;
import com.plaszlo.apps.domain.model.definition.rule.Rule;
import com.plaszlo.apps.domain.model.definition.rule.RuleRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

/**
 * @author plaszlo
 */
@Component
@Slf4j
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class DefinitionSetupService {

  private final ObjectTypeService objectTypeService;
  private final FlexFieldDefinitionRepository flexFieldDefinitionRepository;
  private final RuleRepository ruleRepository;
  private final ObjectTypeRepository objectTypeRepository;
  private final ReconObjectStageReprocessService reprocessService;

  private static final String NO_OBJECT_TYPE = "ObjectType was not provided, setup failed";
  private static final String OBJECT_TYPE_NOT_CREATED = "There was an error creating ObjectType: ";
  private static final String FFIELD_DEFS_NOT_CREATED = "There was an error creating FlexFieldDefinitions: ";
  private static final String RULES_NOT_CREATED = "There was an error creating Rules: ";

  public ResponseEntity<String> processRequest(DefinitionSetupBody request) {
    ObjectType objectType = request.getObjectType();
    List<FlexFieldDefinition> flexFieldDefinitions = request.getFlexFieldDefinitions();
    List<RuleSetupBody> rules = request.getRules();
    List<String> errorList = new ArrayList<>();

    if (objectType == null) {
      return createResponse(NO_OBJECT_TYPE, BAD_REQUEST);
    }
    log.info("ObjectTypes {} received for creation", objectType.getName());
    String error = handleObjectType(objectType);
    if (error != null) {
      errorList.add(error);
    }

    if (!errorList.isEmpty()) {
      return createResponse(OBJECT_TYPE_NOT_CREATED + errorList.toString(), BAD_REQUEST);
    }

    ResultMap<FlexFieldDefinition> definitionResultMap = new ResultMap<>();
    if (!flexFieldDefinitions.isEmpty()) {
      log.info("{} FlexFieldDefinitions received for creation", flexFieldDefinitions.size());

      definitionResultMap = handleFlexFieldDefinitions(flexFieldDefinitions, objectType);
      errorList.addAll(definitionResultMap.getErrorList());
      if (!errorList.isEmpty()) {
        return createResponse(FFIELD_DEFS_NOT_CREATED + errorList.toString(), BAD_REQUEST);
      }
    }

    ResultMap<Rule> ruleResultMap = new ResultMap<>();
    if (!rules.isEmpty()) {
      log.info("{} Rules received for creation", rules.size());

      ruleResultMap = handleRules(rules, objectType);
      errorList.addAll(ruleResultMap.getErrorList());
      if (!errorList.isEmpty()) {
        return createResponse(RULES_NOT_CREATED + errorList.toString(), BAD_REQUEST);
      }
    }

    StringBuilder success = new StringBuilder("Creation successful, created:\r\n1 ObjectType " + objectType.getName());
    if (definitionResultMap.getFieldsToBeCreated() != null) {
      flexFieldDefinitionRepository.saveAll(definitionResultMap.getFieldsToBeCreated());
      success.append("\r\n").append(definitionResultMap.getFieldsToBeCreated().size()).append(" FlexFieldDefinitions");
    }
    if (ruleResultMap.getFieldsToBeCreated() != null) {
      ruleRepository.saveAll(ruleResultMap.getFieldsToBeCreated());
      success.append("\r\n").append(ruleResultMap.getFieldsToBeCreated().size()).append(" Rules");
    }

    return createResponse(success.toString(), OK);
  }

  public ResponseEntity<List<ObjectType>> getObjectTypes() {
    List<ObjectType> objectTypes = objectTypeRepository.findAll();
    return new ResponseEntity<>(objectTypes, OK);
  }

  /**
   * @param objectType
   * @return
   */
  private String handleObjectType(ObjectType objectType) {
    if (!objectType.isValidForCreation()) {
      return "Mandatory fields were not filled properly for the ObjectType";
    }

    Optional<ObjectType> created = objectTypeService.handleObjectTypeCreation(objectType);
    if (created.isPresent()) {
      objectTypeRepository.save(created.get());
      return null;
    }
    return "ObjectType already exists with name " + objectType.getName() + ", did not create new ObjectType";
  }

  /**
   * @param flexFieldDefinitions
   * @param objectType
   * @return
   */
  private ResultMap<FlexFieldDefinition> handleFlexFieldDefinitions(List<FlexFieldDefinition> flexFieldDefinitions,
      ObjectType objectType) {
    List<String> errorList = new ArrayList<>();
    List<FlexFieldDefinition> flexFieldsToBeCreated = new ArrayList<>();
    flexFieldDefinitions.forEach(d -> {
      try {
        if (!d.isValidForCreation()) {
          String error = "Mandatory fields were not filled properly for FlexFieldDefinition with column name "
              + d.getColumnName();
          errorList.add(error);
          return;
        }
        Optional<FlexFieldDefinition> existingFlexField = findExistingFlexFieldDefinition(d);
        if (existingFlexField.isPresent()) {
          errorList.add("FlexFieldDefinition with column name " + d.getColumnName() + "already exists!");
        } else {
          d.setObjectTypeId(objectType.getId());
          flexFieldsToBeCreated.add(d);
        }
      } catch (Exception e) {
        log.error("FlexFieldDefinition creation failed: {}", e.toString());
        errorList.add(e.toString());
      }
    });
    return new ResultMap<>(errorList, flexFieldsToBeCreated);
  }

  /**
   * @param rules
   * @param objectType
   * @return
   */
  private ResultMap<Rule> handleRules(List<RuleSetupBody> rules, ObjectType objectType) {
    List<String> errorList = new ArrayList<>();
    List<Rule> rulesToBeCreated = new ArrayList<>();
    rules.forEach(r -> {
      try {
        if (!r.isValidForCreation()) {
          String error = "Mandatory fields were not filled properly for Rule " + r.getRuleName();
          errorList.add(error);
          return;
        }
        Optional<Rule> existingRule = findExistingRule(r);
        if (existingRule.isPresent()) {
          errorList.add("Rule with name " + r.getRuleName() + "already exists!");
        } else {
          Optional<ObjectType> comparingObjectType = findComparingObjectType(r.getComparingObjectTypeName());
          if (comparingObjectType.isEmpty()) {
            errorList.add("ObjectType with name " + r.getComparingObjectTypeName() + " does not exist!");
            return;
          }
          rulesToBeCreated.add(createRule(r, objectType.getId(), comparingObjectType.get().getId()));
        }
      } catch (Exception e) {
        log.error("Rule creation failed: {}", e.toString());
        errorList.add(e.toString());
      }
    });
    return new ResultMap<>(errorList, rulesToBeCreated);
  }

  /**
   * @param newFlexFieldDefinition
   * @return
   */
  private Optional<FlexFieldDefinition> findExistingFlexFieldDefinition(FlexFieldDefinition newFlexFieldDefinition) {
    return flexFieldDefinitionRepository.findByObjectTypeIdAndColumnName(newFlexFieldDefinition.getObjectTypeId(),
        newFlexFieldDefinition.getColumnName());
  }

  /**
   * @param rule
   * @return
   */
  private Optional<Rule> findExistingRule(Rule rule) {
    return ruleRepository.findByRuleNameIgnoreCaseAndObjectTypeId(rule.getRuleName(), rule.getObjectTypeId());
  }

  /**
   * @param objectTypeName
   * @return
   */
  private Optional<ObjectType> findComparingObjectType(String objectTypeName) {
    return objectTypeRepository.findByNameIgnoreCase(objectTypeName);
  }

  /**
   * @param ruleSetupBody
   * @param objectId
   * @param comparingObjectId
   * @return
   */
  private static Rule createRule(RuleSetupBody ruleSetupBody, Long objectId, Long comparingObjectId) {
    return Rule.builder()
        .objectTypeId(objectId)
        .comparingObjectTypeId(comparingObjectId)
        .ruleDefinition(ruleSetupBody.getRuleDefinition())
        .matchingRuleDefinition(ruleSetupBody.getMatchingRuleDefinition())
        .ruleName(ruleSetupBody.getRuleName())
        .oneToManyFlag(ruleSetupBody.isOneToManyFlag())
        .build();
  }

  /**
   * @param <T>
   */
  @NoArgsConstructor
  @AllArgsConstructor
  @Getter
  private static class ResultMap<T> {

    private List<String> errorList;
    private List<T> fieldsToBeCreated;
  }

}
