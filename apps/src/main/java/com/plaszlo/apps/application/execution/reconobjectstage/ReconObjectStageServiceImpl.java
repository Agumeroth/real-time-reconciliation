package com.plaszlo.apps.application.execution.reconobjectstage;

import com.plaszlo.apps.domain.model.execution.reconobjectstage.ReconObjectStage;
import com.plaszlo.apps.domain.model.execution.reconobjectstage.ReconObjectStage.ReconObjectStageBuilder;
import com.plaszlo.apps.domain.model.execution.reconobjectstage.ReconObjectStageRepository;
import com.plaszlo.apps.domain.shared.DomainEventPublisher;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author plaszlo
 */
@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class ReconObjectStageServiceImpl implements ReconObjectStageService {

  private final ReconObjectStageRepository repository;

  @Override
  public void handle(ConsumerRecord<String, ?> record) {
    log.info("Message received from topic: {}", record.topic());
    process(record);
  }

  @Transactional
  private void process(ConsumerRecord<String, ?> record) {
    ReconObjectStage stage = createStageFromMessage(record).build();
    Optional<ReconObjectStage> existingStage = findExistingStage(stage);
    if (existingStage.isPresent()) {
      log.info("Incoming ReconObjectStage already exists, stopping stage creation.");
      return;
    }

    repository.save(stage);
    log.info("ReconObjectStage created and saved with id {}", stage.getId());
    DomainEventPublisher.publish(ReconObjectStagePersistedEvent.of(stage.getId(), stage));
  }

  private Optional<ReconObjectStage> findExistingStage(ReconObjectStage stage) {
    return repository.findByTopicAndMessage(stage.getTopic(), stage.getMessage());
  }

  private ReconObjectStageBuilder createStageFromMessage(ConsumerRecord<String, ?> record) {
    return ReconObjectStage.builder()
        .message(record.value().toString())
        .topic(record.topic());
  }
}
