package com.plaszlo.apps.application.definition.flexfielddefinition.web;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.plaszlo.apps.domain.model.definition.flexfielddefinition.FlexFieldDefinition;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

@Component
public class FlexFieldDefinitionModelAssembler implements RepresentationModelAssembler<FlexFieldDefinition,
    EntityModel<FlexFieldDefinition>> {

  @Override
  public EntityModel<FlexFieldDefinition> toModel(FlexFieldDefinition entity) {
    return EntityModel.of(entity,
        WebMvcLinkBuilder.linkTo(methodOn(FlexFieldDefinitionController.class)
            .oneFlexFieldDefinition(entity.getObjectTypeId(), entity.getId())).withSelfRel(),
        linkTo(methodOn(FlexFieldDefinitionController.class).allFlexFieldDefinitions(entity.getObjectTypeId()))
            .withRel("flexFieldDefinitions"));
  }
}
