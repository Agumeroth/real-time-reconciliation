package com.plaszlo.apps.application.execution.reconobject;

import com.plaszlo.apps.application.execution.reconobjectstage.ReconObjectStagePersistedEvent;
import com.plaszlo.apps.application.execution.ruleevaluation.RuleEvaluationServiceImpl;
import com.plaszlo.apps.domain.model.execution.ruleexecution.RuleExecutionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author plaszlo
 */
@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ReconObjectServiceImpl implements ReconObjectService {

  private final ReconObjectDerivation derivation;
  private final RetryTemplate retryTemplate;
  private final RuleEvaluationServiceImpl evaluationService;
  private final RuleExecutionRepository executionRepository;

  @Override
  @EventListener
  @Transactional(propagation = Propagation.REQUIRES_NEW)
  public void handleObjectStagePersistedEvent(ReconObjectStagePersistedEvent event) {
    log.info("Handling DomainEvent {} with content: {}", event.getClass().getSimpleName(), event.toString());

    retryTemplate.execute(c -> derivation.derive(event.getReconObjectStage()))
        .forEach(evaluationService::evaluateRulesForObject);

  }
}
