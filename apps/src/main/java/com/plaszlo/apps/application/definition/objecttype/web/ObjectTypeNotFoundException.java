package com.plaszlo.apps.application.definition.objecttype.web;

public class ObjectTypeNotFoundException extends RuntimeException {

  public ObjectTypeNotFoundException(Long id) {
    super("Could not find ObjectType " + id);
  }

  public ObjectTypeNotFoundException(String name) {
    super("Could not find ObjectType by name " + name);
  }
}
