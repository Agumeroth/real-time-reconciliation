package com.plaszlo.apps.application.definition.rule.web;

public class RuleNotFoundException extends RuntimeException {

  public RuleNotFoundException(Long id) {
    super("Could not find Rule " + id);
  }

  public RuleNotFoundException(Long objectTypeId, Long ruleId) {
    super("Could not find Rule " + ruleId + " for ObjectType " + objectTypeId);
  }
}
