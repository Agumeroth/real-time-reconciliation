package com.plaszlo.apps.application.execution.reconobjectstage.reprocess;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author plaszlo
 */
@AllArgsConstructor(onConstructor = @__(@Autowired))
@RestController
public class ReconObjectStageReprocessController {

  private final ReconObjectStageReprocessService service;

  @PostMapping(path = "/stageReprocess")
  public ResponseEntity<String> reprocessStages(@RequestBody ReconObjectStageReprocessBody json) {
    return service.processRequest(json);
  }
}
