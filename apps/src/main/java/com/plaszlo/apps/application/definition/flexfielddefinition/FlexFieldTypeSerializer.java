package com.plaszlo.apps.application.definition.flexfielddefinition;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.plaszlo.apps.domain.model.definition.flexfielddefinition.FlexFieldType;
import java.io.IOException;

/**
 * @author plaszlo
 */
public class FlexFieldTypeSerializer extends JsonSerializer<FlexFieldType> {

  @Override
  public void serialize(FlexFieldType flexFieldType, JsonGenerator jsonGenerator,
      SerializerProvider serializerProvider) throws IOException {
    if (flexFieldType != null) {
      jsonGenerator.writeString(flexFieldType.toString());
    }
  }
}
